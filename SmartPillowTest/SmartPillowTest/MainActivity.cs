﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using System.Text;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Bluetooth;
using System.Linq;
using System.IO;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
//using System.Diagnostics;
using Android.Content.PM;
/* Add Ringbuf library */
using CircularBuffer;
using ControllerDemo;

/* Adding oxyplot */
using OxyPlot.Xamarin.Android;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;


namespace SmartPillowTest
{
    public enum bStateMachineReceivingData : byte { eSM_Step0 = 0, eSM_Step1, eSM_Step2, eSM_Step3, eSM_Step4 };
    public enum MessageIds : byte
    {
        EEGDATA = 1,
        LOG = 2,
        SERIAL_PORT = 3,
        ACCELEROMETER_DATA = 0x4,
    }

    //[Activity(Label = "BluetoothApp", MainLauncher = true, Icon = "@drawable/icon")]
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        BluetoothConnection myConnection = new BluetoothConnection();
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button buttonConnect = FindViewById<Button>(Resource.Id.btnConnect);
            Button buttonDisconnect = FindViewById<Button>(Resource.Id.btnDisconnect);
            Button buttonScan = FindViewById<Button>(Resource.Id.btnScanDevice);
            Button buttonDec = FindViewById<Button>(Resource.Id.buttonDec);
            Button buttonMusic = FindViewById<Button>(Resource.Id.buttonMusic);
            Button buttonInc = FindViewById<Button>(Resource.Id.buttonInc);
            TextView connected = FindViewById<TextView>(Resource.Id.textView1);
            TextView txtA2DPGetState = FindViewById<TextView>(Resource.Id.textA2DPState);
            txtA2DPState = FindViewById<TextView>(Resource.Id.textA2DPStateValue);
            

            /* Play pause command */
            txtPlayPauseCommand = FindViewById<TextView>(Resource.Id.textValueMusicStatus);

            /* For Set Get Product Info */
            txtProductIDValue = FindViewById<TextView>(Resource.Id.textProductIDValue);
            txtProductHWValue = FindViewById<TextView>(Resource.Id.textProductHWValue);
            txtProductFWValue = FindViewById<TextView>(Resource.Id.textProductFWValue);
            txtProductBLValue = FindViewById<TextView>(Resource.Id.textProductBLValue);
            txtProductActiveValue = FindViewById<TextView>(Resource.Id.textProductActiveValue);

            txtProductCmd = FindViewById<TextView>(Resource.Id.textProductCmd);
            txtHWCmd = FindViewById<TextView>(Resource.Id.textHWCmd);
            txtFWCmd = FindViewById<TextView>(Resource.Id.textFWCmd);
            txtBLCmd = FindViewById<TextView>(Resource.Id.textBLCmd);
            txtActiveCmd = FindViewById<TextView>(Resource.Id.textActiveCmd);

            /* For Set Get User Info */
            txtUserNameValue = FindViewById<TextView>(Resource.Id.textUserNameValue);
            txtUserIDValue = FindViewById<TextView>(Resource.Id.textUserIDValue);
            txtUserAccPassValue = FindViewById<TextView>(Resource.Id.textAccountPasswordValue);
            txtUserTelNumValue = FindViewById<TextView>(Resource.Id.textTelephoneNumberValue);
            txtUserMailValue = FindViewById<TextView>(Resource.Id.textMailAddressValue);
            /* Command */
            txtUserNameCmd = FindViewById<TextView>(Resource.Id.textUserNameCmd);
            txtUserIDCmd = FindViewById<TextView>(Resource.Id.textUserIDCmd);
            txtUserAccPassCmd = FindViewById<TextView>(Resource.Id.textAccountPasswordCmd);
            txtUserTelNumCmd = FindViewById<TextView>(Resource.Id.textTelephoneNumberCmd);
            txtUserMailCmd = FindViewById<TextView>(Resource.Id.textMailAddressCmd);

            /* User profile component */
            txtGetSpeaker1 = FindViewById<TextView>(Resource.Id.textGetSpeaker1);
            txtGetSpeaker2 = FindViewById<TextView>(Resource.Id.textGetSpeaker2);
            txtGetSpeaker3 = FindViewById<TextView>(Resource.Id.textGetSpeaker3);
            txtGetSpeaker4 = FindViewById<TextView>(Resource.Id.textGetSpeaker4);
            txtGetSpeaker5 = FindViewById<TextView>(Resource.Id.textGetSpeaker5);
            txtGetVibrator1 = FindViewById<TextView>(Resource.Id.textGetVibrator1);
            txtGetVibrator2 = FindViewById<TextView>(Resource.Id.textGetVibrator2);
            txtGetVibrator3 = FindViewById<TextView>(Resource.Id.textGetVibrator3);
            txtGetVibrator4 = FindViewById<TextView>(Resource.Id.textGetVibrator4);
            txtGetVibrator5 = FindViewById<TextView>(Resource.Id.textGetVibrator5);
            txtValuelLevelSpeaker1 = FindViewById<TextView>(Resource.Id.textValuelLevelSpeaker1);
            txtValueTimingSpeaker1 = FindViewById<TextView>(Resource.Id.textValueTimingSpeaker1);
            txtValuelLevelVibrator1 = FindViewById<TextView>(Resource.Id.textValueLevelVibrator1);
            txtValueTimingVibrator1 = FindViewById<TextView>(Resource.Id.textValueTimingVibrator1);
            txtValuelLevelSpeaker2 = FindViewById<TextView>(Resource.Id.textValuelLevelSpeaker2);
            txtValueTimingSpeaker2 = FindViewById<TextView>(Resource.Id.textValueTimingSpeaker2);
            txtValuelLevelVibrator2 = FindViewById<TextView>(Resource.Id.textValueLevelVibrator2);
            txtValueTimingVibrator2 = FindViewById<TextView>(Resource.Id.textValueTimingVibrator2);
            txtValuelLevelSpeaker3 = FindViewById<TextView>(Resource.Id.textValuelLevelSpeaker3);
            txtValueTimingSpeaker3 = FindViewById<TextView>(Resource.Id.textValueTimingSpeaker3);
            txtValuelLevelVibrator3 = FindViewById<TextView>(Resource.Id.textValueLevelVibrator3);
            txtValueTimingVibrator3 = FindViewById<TextView>(Resource.Id.textValueTimingVibrator3);
            txtValuelLevelSpeaker4 = FindViewById<TextView>(Resource.Id.textValuelLevelSpeaker4);
            txtValueTimingSpeaker4 = FindViewById<TextView>(Resource.Id.textValueTimingSpeaker4);
            txtValuelLevelVibrator4 = FindViewById<TextView>(Resource.Id.textValueLevelVibrator4);
            txtValueTimingVibrator4 = FindViewById<TextView>(Resource.Id.textValueTimingVibrator4);
            txtValuelLevelSpeaker5 = FindViewById<TextView>(Resource.Id.textValuelLevelSpeaker5);
            txtValueTimingSpeaker5 = FindViewById<TextView>(Resource.Id.textValueTimingSpeaker5);
            txtValuelLevelVibrator5 = FindViewById<TextView>(Resource.Id.textValueLevelVibrator5);
            txtValueTimingVibrator5 = FindViewById<TextView>(Resource.Id.textValueTimingVibrator5);
            txtGetTimeUsing = FindViewById<TextView>(Resource.Id.textGetTimeUsing);
            txtValuelTimeUsing = FindViewById<TextView>(Resource.Id.textValuelTimeUsing);
            txtUserProfileCommandActive = FindViewById<TextView>(Resource.Id.textUserProfileCommandActive);
            txtUserProfileCommandDeactive = FindViewById<TextView>(Resource.Id.textUserProfileCommandDeactive);
            txtGetSpeakerLevel = FindViewById<TextView>(Resource.Id.textGetSpeakerLevel);
            txtGetVibratorLevel = FindViewById<TextView>(Resource.Id.textGetVibratorLevel);
            txtValueSpeakerLevel = FindViewById<TextView>(Resource.Id.textValueSpeakerLevel);
            txtValueVibratorLevel = FindViewById<TextView>(Resource.Id.textValueVibratorLevel);

            /* Button */
            Button buttonWeak = FindViewById<Button>(Resource.Id.buttonWeak);
            Button buttonVibrator = FindViewById<Button>(Resource.Id.buttonVibrator);
            Button buttonStrong = FindViewById<Button>(Resource.Id.buttonStrong);
            Button buttonNext = FindViewById<Button>(Resource.Id.buttonNext);
            Button buttonPrevious = FindViewById<Button>(Resource.Id.buttonPrev);
            Button buttonConnectProtocol = FindViewById<Button>(Resource.Id.btnConnectProtocol);
            btnSong1 = FindViewById<Button>(Resource.Id.buttonSong1);
            btnSong2 = FindViewById<Button>(Resource.Id.buttonSong2);
            btnSong3 = FindViewById<Button>(Resource.Id.buttonSong3);
            btnSong4 = FindViewById<Button>(Resource.Id.buttonSong4);
            btnSong5 = FindViewById<Button>(Resource.Id.buttonSong5);
            btnSong6 = FindViewById<Button>(Resource.Id.buttonSong6);

            //byte[] pTest = {49, 50};
            //string pString = "12";
            //pString = Encoding.UTF8.GetString(pTest, 0, pTest.Length);
            //int value = Convert.ToInt32(pString);
            /* Last package sent and recieve */
            tvPackageReceived = FindViewById<TextView>(Resource.Id.textViewLastPackageReceived);
            tvPackageSent = FindViewById<TextView>(Resource.Id.textViewLastPackageSent);

            /* Oxyplot for EEG data */
            Speakerview = FindViewById<PlotView>(Resource.Id.plot_view_Speaker);
            Speakerview.Model = CreatePlotModelSpeaker();
            Vibratorview = FindViewById<PlotView>(Resource.Id.plot_view_Vibrator);
            Vibratorview.Model = CreatePlotModelVibrator();
            Speakerview.InvalidatePlot(true);
            Vibratorview.InvalidatePlot(true);
            this.Controller = new CustomPlotController();

            /* Default UI */
            StrInputDeviceName = FindViewById<EditText>(Resource.Id.textInputDeviceName);
            UIResponse = FindViewById<TextView>(Resource.Id.textUIResponse);
            /* Data result */
            /* UI Init */
            buttonDec.Enabled = false;
            buttonMusic.Enabled = false;
            buttonInc.Enabled = false;
            buttonDisconnect.Enabled = false;
            /* Default */
            buttonWeak.Enabled = false;
            buttonVibrator.Enabled = false;
            buttonStrong.Enabled = false;
            buttonNext.Enabled = false;
            buttonPrevious.Enabled = false;
            buttonConnectProtocol.Enabled = false;


            /* Current time */
            bDateTimeCurrent = DateTime.Now.ToLocalTime();
            string dt_string = bDateTimeCurrent.ToString("yyyy-MM-dd");

            /* Create task update UI */
            UpdateUI();

            //var backingFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "count.txt");
            //var writer = File.CreateText(backingFile);
            //writer.WriteLineAsync("Hello Hai");
            // buttonDisconnect.Enabled = false;
            BluetoothSocket _socket = null;

            txtA2DPGetState.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SendAndDisplayLastPackageSent(ref A2DPCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Get A2DP state event!";
                }
                catch { }
            };
            /* Process button Song selection click */
            btnSong1.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SongSelectionCommand[iUART_DATA + 1] = 1;
                        SendAndDisplayLastPackageSent(ref SongSelectionCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Song 1 button event!";
                }
                catch { }
            };
            btnSong2.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SongSelectionCommand[iUART_DATA + 1] = 2;
                        SendAndDisplayLastPackageSent(ref SongSelectionCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Song 2 button event!";
                }
                catch { }
            };
            btnSong3.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SongSelectionCommand[iUART_DATA + 1] = 3;
                        SendAndDisplayLastPackageSent(ref SongSelectionCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Song 3 button event!";
                }
                catch { }
            };
            btnSong4.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SongSelectionCommand[iUART_DATA + 1] = 4;
                        SendAndDisplayLastPackageSent(ref SongSelectionCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Song 4 button event!";
                }
                catch { }
            };
            btnSong5.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SongSelectionCommand[iUART_DATA + 1] = 5;
                        SendAndDisplayLastPackageSent(ref SongSelectionCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Song 5 button event!";
                }
                catch { }
            };
            btnSong6.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SongSelectionCommand[iUART_DATA + 1] = 6;
                        SendAndDisplayLastPackageSent(ref SongSelectionCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Song 6 button event!";
                }
                catch { }
            };

            /* Process button scan */
            buttonScan.Click += delegate
            {
                /* Define for BLE */
                IBluetoothLE pillow_ble;
                Plugin.BLE.Abstractions.Contracts.IAdapter pillow_adapter;
                ObservableCollection<IDevice> bleDevicesList;
                /* Create connection BLE */
                pillow_ble = CrossBluetoothLE.Current;
                pillow_adapter = CrossBluetoothLE.Current.Adapter;
                bleDevicesList = new ObservableCollection<IDevice>();

                {
                    /* Scan device and connect */
                    pillow_adapter.DeviceDiscovered += (s, a) => bleDevicesList.Add(a.Device);
                    try
                    {
                        bleDevicesList.Clear();
                        pillow_adapter.DeviceDiscovered += (s, a) =>
                        {
                            bleDevicesList.Add(a.Device);
                        };

                        //We have to test if the device is scanning 
                        if (!pillow_ble.Adapter.IsScanning)
                        {
                            pillow_adapter.StartScanningForDevicesAsync();
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            };

            /* Process button click */
            buttonConnect.Click += delegate
            {
                /* Get name of device want to connect */
                string strDeviceConnect;
                strDeviceConnect = StrInputDeviceName.Text;
                /* Update UI */
                buttonDec.Enabled = true;
                buttonMusic.Enabled = true;
                buttonInc.Enabled = true;
                buttonWeak.Enabled = true;
                buttonVibrator.Enabled = true;
                buttonStrong.Enabled = true;
                buttonNext.Enabled = true;
                buttonPrevious.Enabled = true;

                /* Create bluetooth connection */
                /* Using SPP */
                if (USING_SPP_BLE == 0)
                {
                    /* Create connection SPP */
                    myConnection = new BluetoothConnection();
                    myConnection.getAdapter();
                    myConnection.thisAdapter.StartDiscovery();
                }

                /* Using SPP */
                if (USING_SPP_BLE == 0)
                {
                    try
                    {
                        myConnection.getDevice(strDeviceConnect);
                        myConnection.thisDevice.SetPairingConfirmation(false);
                        //   myConnection.thisDevice.Dispose();
                        myConnection.thisDevice.SetPairingConfirmation(true);
                        myConnection.thisDevice.CreateBond();
                    }
                    catch (Exception deviceEX)
                    {
                    }
                    /* Start connect */
                    myConnection.thisAdapter.CancelDiscovery();
                    _socket = myConnection.thisDevice.CreateRfcommSocketToServiceRecord(Java.Util.UUID.FromString("00001101-0000-1000-8000-00805f9b34fb"));
                    myConnection.thisSocket = _socket;

                    /* Get bluetooth socket */
                    _bluetoothSocket = _socket;
                    try
                    {
                        /* Connected state */
                        myConnection.thisSocket.Connect();
                        connected.Text = "Connected!";
                        buttonDisconnect.Enabled = true;
                        buttonConnect.Enabled = false;
                        /* Update UI */
                        buttonDec.Enabled = true;
                        buttonMusic.Enabled = true;
                        buttonInc.Enabled = true;
                        buttonWeak.Enabled = true;
                        buttonVibrator.Enabled = true;
                        buttonStrong.Enabled = true;
                        buttonNext.Enabled = true;
                        buttonPrevious.Enabled = true;
                        buttonConnectProtocol.Enabled = true;

                        /* Call direct listener */
                        _cancellationSource = new CancellationTokenSource();
                        var result = Connect(_cancellationSource.Token);
                        /* Update UIResponse */
                        UIResponse.Text = "[UI]: Connected event!";

                        /* Clear flag */
                        bRunningProcess = false;
                        bRunningGet1stPackage = false;
                        bFlagGetTimeStart = false;

                        /* Create timer */
                        bTimerQuerryReceiveData = new System.Timers.Timer();
                        bTimerQuerryReceiveData.Interval = 1;//10ms
                        bTimerQuerryReceiveData.Elapsed += OnTimedEvent;
                        bTimerQuerryReceiveData.Enabled = true;

                        /* Clear flag */
                        bRunningProcess = false;
                        bRunningGet1stPackage = false;
                        /* Reset variable data */
                        bNumberBytesPerPackageReceivedCounter = 0;
                        bNumberBytesReceivedCounter = 0;
                        bNumberPackageReceivedCounter = 0;
                        bNumberBytesLastPackage = 0;
                        bFlagGetTimeStart = false;
                        /* Clear all buffer */
                        Array.Clear(ringBufReceivedData, 0, 2048);
                        Array.Clear(cacheBufReceiveData, 0, 2048);
                        /* Reset counter */
                        bIndex = 0;
                        bCheck1stHeaderByte = 0;
                        bCounterGetPackageHeader = 0;
                        bFlagStartGetHeader = 0;
                        bStateFindPackageInRingBuffer = bStateMachineReceivingData.eSM_Step0;
                        bDetectErrorFail = 0;
                        bLastSequence = bCurrentSequence - 1;
                    }
                    catch (Exception CloseEX)
                    {

                    }
                }
            };

            buttonDisconnect.Click += delegate
            {
                try
                {
                    /* Clear process flag */
                    bRunningProcess = false;
                    //  buttonDisconnect.Enabled = false;
                    buttonConnect.Enabled = true;
                    //listenThread.Abort();
                    myConnection.thisDevice.Dispose();
                    myConnection.thisSocket.OutputStream.WriteByte(187);
                    myConnection.thisSocket.OutputStream.Close();
                    myConnection.thisSocket.Close();
                    myConnection = new BluetoothConnection();
                    _socket = null;
                    connected.Text = "Disconnected!";
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Disconnected event!";

                    /* Change some UI */
                    buttonDec.Enabled = false;
                    buttonMusic.Enabled = false;
                    buttonInc.Enabled = false;
                    buttonWeak.Enabled = false;
                    buttonVibrator.Enabled = false;
                    buttonStrong.Enabled = false;
                    buttonDisconnect.Enabled = false;
                    buttonNext.Enabled = false;
                    buttonPrevious.Enabled = false;
                    /* Reset all value */
                    buttonConnectProtocol.Enabled = false;
                    buttonConnectProtocol.Text = "CONNECT PROTOCOL";
                    txtProductIDValue.Text = "----";
                    txtProductHWValue.Text = "----";
                    txtProductFWValue.Text = "----";
                    txtProductBLValue.Text = "----";
                    bNumberBytesPerPackageReceivedCounter = 0;

                    /* Clear flag */
                    bRunningProcess = false;
                    bRunningGet1stPackage = false;
                    /* Reset variable data */
                    bNumberBytesPerPackageReceivedCounter = 0;
                    bNumberBytesReceivedCounter = 0;
                    bNumberPackageReceivedCounter = 0;
                    bNumberBytesLastPackage = 0;
                    bFlagGetTimeStart = false;
                    /* Clear all buffer */
                    Array.Clear(ringBufReceivedData, 0, 2048);
                    Array.Clear(cacheBufReceiveData, 0, 2048);
                    /* Reset counter */
                    bIndex = 0;
                    bCheck1stHeaderByte = 0;
                    bCounterGetPackageHeader = 0;
                    bFlagStartGetHeader = 0;
                    bStateFindPackageInRingBuffer = bStateMachineReceivingData.eSM_Step0;
                    bDetectErrorFail = 0;
                    bLastSequence = bCurrentSequence - 1;
                    bTimerQuerryReceiveData.Enabled = false;
                }
                catch { }
            };

            /* Long Press to set Product ID */
            txtProductIDValue.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Product ID text Long click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_ProductID;
                    GetUserInputData();
                }
                catch { }
            };

            /* Get Information click */
            txtPlayPauseCommand.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtPlayPauseCommand.Text == "Playing")
                        {
                            /* Send Pause command */
                            SendAndDisplayLastPackageSent(ref MediaPlayPauseCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:PlayPause Cmd Get Click event!";
                            /* Change content and color */
                            txtPlayPauseCommand.Text = "Pause";
                            txtPlayPauseCommand.SetTextColor(Android.Graphics.Color.Red);
                        }
                        else
                        {
                            /* Send Play command */
                            SendAndDisplayLastPackageSent(ref MediaPlayPauseCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:PlayPause Cmd Get Click event!";
                            /* Change content and color */
                            txtPlayPauseCommand.Text = "Playing";
                            txtPlayPauseCommand.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };

            /* Get value speaker level */
            txtGetSpeakerLevel.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetSpeakerLevel.Text == "Get")
                        {
                            /* Send get command */
                            SendAndDisplayLastPackageSent(ref GetSpeakerLevel);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Value Speaker Cmd Get Click event!";
                            /* Change content and color */
                            txtGetSpeakerLevel.Text = "Get";
                            txtGetSpeakerLevel.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValueSpeakerLevel.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_SpeakerLevel, ref SetSpeakerLevel, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref SetSpeakerLevel);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Value Speaker ID Cmd Set Click event!";
                            /* Change content and color */
                            txtGetSpeakerLevel.Text = "Get";
                            txtGetSpeakerLevel.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set level speaker phase 1 */
            txtValueSpeakerLevel.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Speaker Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_SpeakerLevel;
                    GetUserInputData();
                }
                catch { }
            };

            /* Get value speaker level */
            txtGetVibratorLevel.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetVibratorLevel.Text == "Get")
                        {
                            /* Send get command */
                            SendAndDisplayLastPackageSent(ref GetVibratorLevel);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Value Vibrator Cmd Get Click event!";
                            /* Change content and color */
                            txtGetVibratorLevel.Text = "Get";
                            txtGetVibratorLevel.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValueVibratorLevel.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_VibratorLevel, ref SetVibratorLevel, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref SetVibratorLevel);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Value Vibrator ID Cmd Set Click event!";
                            /* Change content and color */
                            txtGetVibratorLevel.Text = "Get";
                            txtGetVibratorLevel.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set level speaker phase 1 */
            txtValueVibratorLevel.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Vibrator Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_VibratorLevel;
                    GetUserInputData();
                }
                catch { }
            };

            /* Get Information click */
            txtProductCmd.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtProductCmd.Text == "Get")
                        {
                            /* Send get command */
                            SendAndDisplayLastPackageSent(ref InfoGetProductIDCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Product ID Cmd Get Click event!";
                            /* Change content and color */
                            txtProductCmd.Text = "Get";
                            txtProductCmd.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtProductIDValue.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_ProductID, ref InfoSetProductIDCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetProductIDCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Product ID Cmd Set Click event!";
                            /* Change content and color */
                            txtProductCmd.Text = "Get";
                            txtProductCmd.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Get Information click */
            txtHWCmd.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send connect command */
                        SendAndDisplayLastPackageSent(ref InfoGetHWVersionCommand);
                        /* Update UIResponse */
                        UIResponse.Text = "[UI]:HW Version Cmd Click event!";
                    }
                }
                catch { }
            };
            /* Get Information click */
            txtFWCmd.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send connect command */
                        SendAndDisplayLastPackageSent(ref InfoGetFWVersionCommand);
                        /* Update UIResponse */
                        UIResponse.Text = "[UI]:FW Version Cmd Click event!";
                    }
                }
                catch { }
            };
            /* Get Information click */
            txtBLCmd.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send connect command */
                        SendAndDisplayLastPackageSent(ref InfoGetBLVersionCommand);
                        /* Update UIResponse */
                        UIResponse.Text = "[UI]:BL Version Cmd Click event!";
                    }
                }
                catch { }
            };
            
            /* Product Active */
            txtActiveCmd.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtActiveCmd.Text == "Get")
                        {
                            /* Send get command */
                            SendAndDisplayLastPackageSent(ref InfoGetActiveCodeCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Active code Cmd Get Click event!";
                            /* Change content and color */
                            txtActiveCmd.Text = "Get";
                            txtActiveCmd.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtProductActiveValue.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_ProductActiveCode, ref InfoSetProductActiveCode, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetProductActiveCode);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Active code Cmd Set Click event!";
                            /* Change content and color */
                            txtActiveCmd.Text = "Get";
                            txtActiveCmd.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set User Name */
            txtProductActiveValue.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Active code text Long click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_ProductActiveCode;
                    GetUserInputData();
                }
                catch { }
            };

            /* User name */
            txtUserNameCmd.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtUserNameCmd.Text == "Get")
                        {
                            /* Send get command */
                            SendAndDisplayLastPackageSent(ref InfoGetUserNameCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:User Name Cmd Get Click event!";
                            /* Change content and color */
                            txtUserNameCmd.Text = "Get";
                            txtUserNameCmd.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtUserNameValue.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_UserName, ref InfoSetUserNameCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetUserNameCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:User Name Cmd Set Click event!";
                            /* Change content and color */
                            txtUserNameCmd.Text = "Get";
                            txtUserNameCmd.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set User Name */
            txtUserNameValue.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:User Name text Long click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_UserName;
                    GetUserInputData();
                }
                catch { }
            };

            /* User name */
            txtUserIDCmd.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtUserIDCmd.Text == "Get")
                        {
                            /* Send get command */
                            SendAndDisplayLastPackageSent(ref InfoGetUserIDCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:User ID Cmd Get Click event!";
                            /* Change content and color */
                            txtUserIDCmd.Text = "Get";
                            txtUserIDCmd.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtUserIDValue.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_UserID, ref InfoSetUserIDCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetUserIDCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:User ID Cmd Set Click event!";
                            /* Change content and color */
                            txtUserIDCmd.Text = "Get";
                            txtUserIDCmd.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set User Name */
            txtUserIDValue.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:User ID text Long click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_UserID;
                    GetUserInputData();
                }
                catch { }
            };

            /* Acc Pass */
            txtUserAccPassCmd.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtUserAccPassCmd.Text == "Get")
                        {
                            /* Send get command */
                            SendAndDisplayLastPackageSent(ref InfoGetUserAccPassCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:User AccPass Cmd Get Click event!";
                            /* Change content and color */
                            txtUserAccPassCmd.Text = "Get";
                            txtUserAccPassCmd.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtUserAccPassValue.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_UserPass, ref InfoSetUserAccPassCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetUserAccPassCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:User AccPass Cmd Set Click event!";
                            /* Change content and color */
                            txtUserAccPassCmd.Text = "Get";
                            txtUserAccPassCmd.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set Acc & Pass */
            txtUserAccPassValue.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:User Acc Pass text Long click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_UserPass;
                    GetUserInputData();
                }
                catch { }
            };

            /* Tel Number */
            txtUserTelNumCmd.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtUserTelNumCmd.Text == "Get")
                        {
                            /* Send get command */
                            SendAndDisplayLastPackageSent(ref InfoGetUserTeleNumbCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:User Telephone Number Cmd Get Click event!";
                            /* Change content and color */
                            txtUserTelNumCmd.Text = "Get";
                            txtUserTelNumCmd.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtUserTelNumValue.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_UserTel, ref InfoSetUserTeleNumbCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetUserTeleNumbCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:User Telephone Number Cmd Set Click event!";
                            /* Change content and color */
                            txtUserTelNumCmd.Text = "Get";
                            txtUserTelNumCmd.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set Tel&Numb */
            txtUserTelNumValue.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:User TelNumb text Long click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_UserTel;
                    GetUserInputData();
                }
                catch { }
            };

            /* Mail command */
            txtUserMailCmd.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtUserMailCmd.Text == "Get")
                        {
                            /* Send get command */
                            SendAndDisplayLastPackageSent(ref InfoGetUserMailCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:User Mail Cmd Get Click event!";
                            /* Change content and color */
                            txtUserMailCmd.Text = "Get";
                            txtUserMailCmd.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtUserMailValue.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_UserMail, ref InfoSetUserMailCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetUserMailCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:User Mail Cmd Set Click event!";
                            /* Change content and color */
                            txtUserMailCmd.Text = "Get";
                            txtUserMailCmd.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set Mail */
            txtUserMailValue.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:User Mail text Long click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_UserMail;
                    GetUserInputData();
                }
                catch { }
            };

            /* User Profile button */
            txtUserProfileCommandActive.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send get command */
                        SendAndDisplayLastPackageSent(ref InfoUserProfileActive);
                        /* Update UIResponse */
                        UIResponse.Text = "[UI]: User Profile Active Cmd Click event!";
                        txtUserProfileCommandActive.SetTextColor(Android.Graphics.Color.Blue);
                        txtUserProfileCommandDeactive.SetTextColor(Android.Graphics.Color.Black);
                    }
                }
                catch { }
            };

            txtUserProfileCommandDeactive.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send get command */
                        SendAndDisplayLastPackageSent(ref InfoUserProfileDeactive);
                        /* Update UIResponse */
                        UIResponse.Text = "[UI]: User Profile Deactive Cmd Click event!";
                        txtUserProfileCommandDeactive.SetTextColor(Android.Graphics.Color.Blue);
                        txtUserProfileCommandActive.SetTextColor(Android.Graphics.Color.Black);
                    }
                }
                catch { }
            };
            /* Product Active */
            txtGetTimeUsing.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetTimeUsing.Text == "Get")
                        {
                            /* Send get command */
                            SendAndDisplayLastPackageSent(ref InfoGetPhaseTimeUsingCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]: Time Using Cmd Get Click event!";
                            /* Change content and color */
                            txtGetTimeUsing.Text = "Get";
                            txtGetTimeUsing.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValuelTimeUsing.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_TimeUsing, ref InfoSetPhaseTimeUsingCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetPhaseTimeUsingCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Time Using Cmd Set Click event!";
                            /* Change content and color */
                            txtGetTimeUsing.Text = "Get";
                            txtGetTimeUsing.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set level speaker phase 1 */
            txtValuelTimeUsing.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Time Using Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_TimeUsing;
                    GetUserInputData();
                }
                catch { }
            };

            /* Get Information click */
            txtGetSpeaker1.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetSpeaker1.Text == "Get")
                        {
                            /* Send get command */
                            InfoGetPhaseXSPKCommand[9] = 1;
                            SendAndDisplayLastPackageSent(ref InfoGetPhaseXSPKCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 1 Speaker Cmd Get Click event!";
                            /* Change content and color */
                            txtGetSpeaker1.Text = "Get";
                            txtGetSpeaker1.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValuelLevelSpeaker1.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_SpeakerLevelPhase1, ref InfoSetPhaseXSPKCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetPhaseXSPKCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 1 Speaker Cmd Set Click event!";
                            /* Change content and color */
                            txtGetSpeaker1.Text = "Get";
                            txtGetSpeaker1.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set level speaker phase 1 */
            txtValuelLevelSpeaker1.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Speaker 1 Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_SpeakerLevelPhase1;
                    GetUserInputData();
                }
                catch { }
            };
            /* Long Press to set timing speaker phase 1 */
            txtValueTimingSpeaker1.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Speaker 1 Set timing click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_SpeakerTimingPhase1;
                    GetUserInputData();
                }
                catch { }
            };

            /* Phase 2 */
            txtGetSpeaker2.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetSpeaker2.Text == "Get")
                        {
                            /* Send get command */
                            InfoGetPhaseXSPKCommand[9] = 2;
                            SendAndDisplayLastPackageSent(ref InfoGetPhaseXSPKCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 2 Speaker Cmd Get Click event!";
                            /* Change content and color */
                            txtGetSpeaker2.Text = "Get";
                            txtGetSpeaker2.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValuelLevelSpeaker2.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_SpeakerLevelPhase2, ref InfoSetPhaseXSPKCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetPhaseXSPKCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 2 Speaker Cmd Set Click event!";
                            /* Change content and color */
                            txtGetSpeaker2.Text = "Get";
                            txtGetSpeaker2.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set level speaker phase 2 */
            txtValuelLevelSpeaker2.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Speaker 2 Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_SpeakerLevelPhase2;
                    GetUserInputData();
                }
                catch { }
            };
            /* Long Press to set timing speaker phase 2 */
            txtValueTimingSpeaker2.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Speaker 2 Set timing click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_SpeakerTimingPhase2;
                    GetUserInputData();
                }
                catch { }
            };

            /* Phase 3 */
            txtGetSpeaker3.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetSpeaker3.Text == "Get")
                        {
                            /* Send get command */
                            InfoGetPhaseXSPKCommand[9] = 3;
                            SendAndDisplayLastPackageSent(ref InfoGetPhaseXSPKCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 3 Speaker Cmd Get Click event!";
                            /* Change content and color */
                            txtGetSpeaker3.Text = "Get";
                            txtGetSpeaker3.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValuelLevelSpeaker3.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_SpeakerLevelPhase3, ref InfoSetPhaseXSPKCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetPhaseXSPKCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 3 Speaker Cmd Set Click event!";
                            /* Change content and color */
                            txtGetSpeaker3.Text = "Get";
                            txtGetSpeaker3.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set speaker phase 3 */
            txtValuelLevelSpeaker3.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Speaker 3 Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_SpeakerLevelPhase3;
                    GetUserInputData();
                }
                catch { }
            };
            /* Long Press to set timing speaker phase 3 */
            txtValueTimingSpeaker3.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Speaker 3 Set timing click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_SpeakerTimingPhase3;
                    GetUserInputData();
                }
                catch { }
            };

            /* Phase 4 */
            txtGetSpeaker4.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetSpeaker4.Text == "Get")
                        {
                            /* Send get command */
                            InfoGetPhaseXSPKCommand[9] = 4;
                            SendAndDisplayLastPackageSent(ref InfoGetPhaseXSPKCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 4 Speaker Cmd Get Click event!";
                            /* Change content and color */
                            txtGetSpeaker4.Text = "Get";
                            txtGetSpeaker4.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValuelLevelSpeaker4.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_SpeakerLevelPhase4, ref InfoSetPhaseXSPKCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetPhaseXSPKCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 4 Speaker Cmd Set Click event!";
                            /* Change content and color */
                            txtGetSpeaker4.Text = "Get";
                            txtGetSpeaker4.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set speaker phase 4 */
            txtValuelLevelSpeaker4.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Speaker 4 Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_SpeakerLevelPhase4;
                    GetUserInputData();
                }
                catch { }
            };
            /* Long Press to set timing speaker phase 1 */
            txtValueTimingSpeaker4.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Speaker 4 Set timing click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_SpeakerTimingPhase4;
                    GetUserInputData();
                }
                catch { }
            };

            /* Phase 5 */
            txtGetSpeaker5.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetSpeaker5.Text == "Get")
                        {
                            /* Send get command */
                            InfoGetPhaseXSPKCommand[9] = 5;
                            SendAndDisplayLastPackageSent(ref InfoGetPhaseXSPKCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 5 Speaker Cmd Get Click event!";
                            /* Change content and color */
                            txtGetSpeaker5.Text = "Get";
                            txtGetSpeaker5.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValuelLevelSpeaker5.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_SpeakerLevelPhase5, ref InfoSetPhaseXSPKCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetPhaseXSPKCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 5 Speaker Cmd Set Click event!";
                            /* Change content and color */
                            txtGetSpeaker5.Text = "Get";
                            txtGetSpeaker5.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set speaker phase 5 */
            txtValuelLevelSpeaker5.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Speaker 5 Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_SpeakerLevelPhase5;
                    GetUserInputData();
                }
                catch { }
            };
            /* Long Press to set timing speaker phase 1 */
            txtValueTimingSpeaker5.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Speaker 5 Set timing click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_SpeakerTimingPhase5;
                    GetUserInputData();
                }
                catch { }
            };


            /**/
            txtGetVibrator1.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetVibrator1.Text == "Get")
                        {
                            /* Send get command */
                            InfoGetPhaseXVIBCommand[9] = 1;
                            SendAndDisplayLastPackageSent(ref InfoGetPhaseXVIBCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 1 Vibrator Cmd Get Click event!";
                            /* Change content and color */
                            txtGetVibrator1.Text = "Get";
                            txtGetVibrator1.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValuelLevelVibrator1.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_VibratorLevelPhase1, ref InfoSetPhaseXVIBCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetPhaseXVIBCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 1 Vibrator Cmd Set Click event!";
                            /* Change content and color */
                            txtGetVibrator1.Text = "Get";
                            txtGetVibrator1.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set level vibrator phase 1 */
            txtValuelLevelVibrator1.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Vibrator 1 Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_VibratorLevelPhase1;
                    GetUserInputData();
                }
                catch { }
            };
            /* Long Press to set timing vibrator phase 1 */
            txtValueTimingVibrator1.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Vibrator 1 Set timing click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_VibratorTimingPhase1;
                    GetUserInputData();
                }
                catch { }
            };
            /* Phase 2 */
            txtGetVibrator2.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetVibrator2.Text == "Get")
                        {
                            /* Send get command */
                            InfoGetPhaseXVIBCommand[9] = 2;
                            SendAndDisplayLastPackageSent(ref InfoGetPhaseXVIBCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 2 Vibrator Cmd Get Click event!";
                            /* Change content and color */
                            txtGetVibrator2.Text = "Get";
                            txtGetVibrator2.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValuelLevelVibrator2.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_VibratorLevelPhase2, ref InfoSetPhaseXVIBCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetPhaseXVIBCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 2 Vibrator Cmd Set Click event!";
                            /* Change content and color */
                            txtGetVibrator2.Text = "Get";
                            txtGetVibrator2.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set level vibrator phase 2 */
            txtValuelLevelVibrator2.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Vibrator 2 Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_VibratorLevelPhase2;
                    GetUserInputData();
                }
                catch { }
            };
            /* Long Press to set timing vibrator phase 2 */
            txtValueTimingVibrator2.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Vibrator 2 Set timing click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_VibratorTimingPhase2;
                    GetUserInputData();
                }
                catch { }
            };

            /* Phase 3 */
            txtGetVibrator3.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetVibrator3.Text == "Get")
                        {
                            /* Send get command */
                            InfoGetPhaseXVIBCommand[9] = 3;
                            SendAndDisplayLastPackageSent(ref InfoGetPhaseXVIBCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 3 Vibrator Cmd Get Click event!";
                            /* Change content and color */
                            txtGetVibrator3.Text = "Get";
                            txtGetVibrator3.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValuelLevelVibrator3.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_VibratorLevelPhase3, ref InfoSetPhaseXVIBCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetPhaseXVIBCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 3 Vibrator Cmd Set Click event!";
                            /* Change content and color */
                            txtGetVibrator3.Text = "Get";
                            txtGetVibrator3.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set level vibrator phase 3 */
            txtValuelLevelVibrator3.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Vibrator 3 Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_VibratorLevelPhase3;
                    GetUserInputData();
                }
                catch { }
            };
            /* Long Press to set timing vibrator phase 3 */
            txtValueTimingVibrator3.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Vibrator 3 Set timing click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_VibratorTimingPhase3;
                    GetUserInputData();
                }
                catch { }
            };

            /* Phase 4 */
            txtGetVibrator4.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetVibrator4.Text == "Get")
                        {
                            /* Send get command */
                            InfoGetPhaseXVIBCommand[9] = 4;
                            SendAndDisplayLastPackageSent(ref InfoGetPhaseXVIBCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 4 Vibrator Cmd Get Click event!";
                            /* Change content and color */
                            txtGetVibrator4.Text = "Get";
                            txtGetVibrator4.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValuelLevelVibrator4.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_VibratorLevelPhase4, ref InfoSetPhaseXVIBCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetPhaseXVIBCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 4 Vibrator Cmd Set Click event!";
                            /* Change content and color */
                            txtGetVibrator4.Text = "Get";
                            txtGetVibrator4.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set level vibrator phase 4 */
            txtValuelLevelVibrator4.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Vibrator 4 Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_VibratorLevelPhase4;
                    GetUserInputData();
                }
                catch { }
            };
            /* Long Press to set timing vibrator phase 4 */
            txtValueTimingVibrator4.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Vibrator 4 Set timing click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_VibratorTimingPhase4;
                    GetUserInputData();
                }
                catch { }
            };

            /* Phase 5*/
            txtGetVibrator5.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (txtGetVibrator5.Text == "Get")
                        {
                            /* Send get command */
                            InfoGetPhaseXVIBCommand[9] = 5;
                            SendAndDisplayLastPackageSent(ref InfoGetPhaseXVIBCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 5 Vibrator Cmd Get Click event!";
                            /* Change content and color */
                            txtGetVibrator5.Text = "Get";
                            txtGetVibrator5.SetTextColor(Android.Graphics.Color.Blue);
                        }
                        else
                        {
                            /* Send set command */
                            StrDataAddToFrame = Encoding.ASCII.GetBytes(txtValuelLevelVibrator5.Text);
                            ParseDataInputAndAddToFrame(Component_Index.CoIdx_VibratorLevelPhase5, ref InfoSetPhaseXVIBCommand, ref StrDataAddToFrame);
                            SendAndDisplayLastPackageSent(ref InfoSetPhaseXVIBCommand);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Phase 5 Vibrator Cmd Set Click event!";
                            /* Change content and color */
                            txtGetVibrator5.Text = "Get";
                            txtGetVibrator5.SetTextColor(Android.Graphics.Color.Blue);
                        };
                    }
                }
                catch { }
            };
            /* Long Press to set level vibrator phase 5 */
            txtValuelLevelVibrator5.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Vibrator 5 Set level click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_VibratorLevelPhase5;
                    GetUserInputData();
                }
                catch { }
            };
            /* Long Press to set timing vibrator phase 5 */
            txtValueTimingVibrator5.Click += delegate
            {
                try
                {
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Vibrator 5 Set timing click event!";
                    /* Update component index */
                    pComIndex = Component_Index.CoIdx_VibratorTimingPhase5;
                    GetUserInputData();
                }
                catch { }
            };


            /* Button Connect Protocol action */
            buttonConnectProtocol.Click += delegate
            {
                try
                {
                    /* Start running */
                    bRunningProcess = true;
                    if (_bluetoothSocket.IsConnected)
                    {
                        if (buttonConnectProtocol.Text == "CONNECT PROTOCOL")
                        {
                            buttonConnectProtocol.Text = "DISCONNECT PROTOCOL";
                            /* Send connect command */
                            SendAndDisplayLastPackageSent(ref ConnectProtocol);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Con Protocol event!";
                        }
                        else
                        {
                            buttonConnectProtocol.Text = "CONNECT PROTOCOL";
                            /* Send Disconnect command */
                            SendAndDisplayLastPackageSent(ref DisconnectProtocol);
                            /* Update UIResponse */
                            UIResponse.Text = "[UI]:Dis Protocol event!";
                        }
                    }
                }
                catch { }
            };

            /* Page Dec action */
            buttonDec.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SendAndDisplayLastPackageSent(ref MediaDecreaseVolumeCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Dec button event!";
                }
                catch { }
            };

            /* Page Next action */
            buttonNext.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SendAndDisplayLastPackageSent(ref MediaNextCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Next button event!";
                }
                catch { }
            };

            /* Page Previous action */
            buttonPrevious.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SendAndDisplayLastPackageSent(ref MediaPreviousCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Previous button event!";
                }
                catch { }
            };
            /* Page Music action */
            buttonMusic.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SendAndDisplayLastPackageSent(ref MediaPlayStopCommand);
                        if (buttonMusic.Text == "MUS_ON")
                        {
                            buttonMusic.Text = "MUS_OFF";
                        }
                        else
                        {
                            buttonMusic.Text = "MUS_ON";
                        }
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Music button event!";
                }
                catch { }
            };

            /* Page Inc action */
            buttonInc.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SendAndDisplayLastPackageSent(ref MediaIncreaseVolumeCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Inc button event!";
                }
                catch { }
            };

            /* Page Weak action */
            buttonWeak.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SendAndDisplayLastPackageSent(ref VibratorWeakCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Weak button event!";
                }
                catch { }
            };

            /* Page Vibrator action */
            buttonVibrator.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SendAndDisplayLastPackageSent(ref VibratorPlayStopCommand);
                    }
                    if (buttonVibrator.Text == "VIB_ON")
                    {
                        buttonVibrator.Text = "VIB_OFF";
                    }
                    else
                    {
                        buttonVibrator.Text = "VIB_ON";
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Vibrator button event!";
                }
                catch { }
            };

            /* Page Strong action */
            buttonStrong.Click += delegate
            {
                try
                {
                    if (_bluetoothSocket.IsConnected)
                    {
                        /* Send Disconnect command */
                        SendAndDisplayLastPackageSent(ref VibratorStrongCommand);
                    }
                    /* Update UIResponse */
                    UIResponse.Text = "[UI]:Strong button event!";
                }
                catch { }
            };
        }
        private void OnTimedEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            /* In timer */
            if (bFlagSendData == true)
            {
                /* Increase counter */
                uCounterTimer++;
                /* Check interval timeout */
                if (uCounterTimer >= 100)/* 30ms */
                {
                    bFlagIntervalTimeout = true;
                    uCounterTimer = 0;
                }
            }
            /* In timer */
        }

        private void ParseDataInputAndAddToFrame(Component_Index component_Index, ref byte[] pPackageFrame, ref byte[] pDataContent)
        {
            try
            {
                int value;
                string pValueString;
                /* Add header */
                pPackageFrame[iUART_PREAM_OFFSET_1] = (byte)'A';
                pPackageFrame[iUART_PREAM_OFFSET_2] = (byte)'T';
                pPackageFrame[iUART_PREAM_OFFSET_3] = (byte)'D';
                pPackageFrame[iUART_PREAM_OFFSET_4] = (byte)'W';

                /* Fixed data size */
                switch (component_Index)
                {
                    case Component_Index.CoIdx_ProductID:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_PRODUCT_ID;
                        /* Add length byte */
                        //Array.Copy(BitConverter.GetBytes(PRODUCT_ID_SIZE), 0, pPackageFrame, iUART_LENGTH_OFFSET, LENGTH_SIZE);
                        /* Add data */
                        Array.Copy(pDataContent, 0, pPackageFrame, iUART_DATA, PRODUCT_ID_SIZE);
                        /* Add index package */
                        break;
                    case Component_Index.CoIdx_ProductActiveCode:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_PRODUCT_ACTIVE;
                        /* Add length byte */
                        //Array.Copy(BitConverter.GetBytes(PRODUCT_ID_SIZE), 0, pPackageFrame, iUART_LENGTH_OFFSET, LENGTH_SIZE);
                        /* Add data */
                        Array.Copy(pDataContent, 0, pPackageFrame, iUART_DATA, SIZE_ACTIVE_CODE);
                        /* Add index package */
                        break;
                    case Component_Index.CoIdx_UserName:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_USER_NAME;
                        /* Add length byte */
                        //Array.Copy(BitConverter.GetBytes(SIZE_USER_NAME), 0, pPackageFrame, iUART_LENGTH_OFFSET, LENGTH_SIZE);
                        /* Add data */
                        if (pDataContent.Length >= SIZE_USER_NAME)
                        {
                            Array.Copy(pDataContent, 0, pPackageFrame, iUART_DATA, SIZE_USER_NAME);
                        }
                        else
                        {
                            Array.Copy(pDataContent, 0, pPackageFrame, iUART_DATA, pDataContent.Length);
                        }
                        /* Add index package */
                        break;
                    case Component_Index.CoIdx_UserID:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_USER_ID;
                        /* Add length byte */
                        //Array.Copy(BitConverter.GetBytes(SIZE_ID_ACCOUNT), 0, pPackageFrame, iUART_LENGTH_OFFSET, LENGTH_SIZE);
                        /* Add data */
                        if (pDataContent.Length >= SIZE_ID_ACCOUNT)
                        {
                            Array.Copy(pDataContent, 0, pPackageFrame, iUART_DATA, SIZE_ID_ACCOUNT);
                        }
                        else
                        {
                            Array.Copy(pDataContent, 0, pPackageFrame, iUART_DATA, pDataContent.Length);
                        }
                        /* Add index package */
                        break;
                    case Component_Index.CoIdx_UserMail:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_USER_MAIL;
                        /* Add length byte */
                        //Array.Copy(BitConverter.GetBytes(SIZE_USER_MAIL), 0, pPackageFrame, iUART_LENGTH_OFFSET, LENGTH_SIZE);
                        /* Add data */
                        if (pDataContent.Length >= SIZE_USER_MAIL)
                        {
                            Array.Copy(pDataContent, 0, pPackageFrame, iUART_DATA, SIZE_USER_MAIL);
                        }
                        else
                        {
                            Array.Copy(pDataContent, 0, pPackageFrame, iUART_DATA, pDataContent.Length);
                        }
                        /* Add index package */
                        break;
                    case Component_Index.CoIdx_UserPass:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_USER_ACCPASS;
                        /* Add length byte */
                        //Array.Copy(BitConverter.GetBytes(SIZE_ACCOUNT_PASSWORD), 0, pPackageFrame, iUART_LENGTH_OFFSET, LENGTH_SIZE);
                        /* Add data */
                        if (pDataContent.Length >= SIZE_ACCOUNT_PASSWORD)
                        {
                            Array.Copy(pDataContent, 0, pPackageFrame, iUART_DATA, SIZE_ACCOUNT_PASSWORD);
                        }
                        else
                        {
                            Array.Copy(pDataContent, 0, pPackageFrame, iUART_DATA, pDataContent.Length);
                        }
                        break;
                    case Component_Index.CoIdx_UserTel:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_USER_TELE_NUM;
                        /* Add length byte */
                        //Array.Copy(BitConverter.GetBytes(SIZE_USER_TELEPHONE), 0, pPackageFrame, iUART_LENGTH_OFFSET, LENGTH_SIZE);
                        /* Add data */
                        if (pDataContent.Length >= SIZE_USER_TELEPHONE)
                        {
                            Array.Copy(pDataContent, 0, pPackageFrame, iUART_DATA, SIZE_USER_TELEPHONE);
                        }
                        else
                        {
                            Array.Copy(pDataContent, 0, pPackageFrame, iUART_DATA, pDataContent.Length);
                        }
                        break;
                    /* Time using */
                    case Component_Index.CoIdx_TimeUsing:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_TIME_USING;
                        /* Convert and Add data */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;
                    /* Speaker level set */
                    case Component_Index.CoIdx_SpeakerLevel:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_MEDIA_SET_SPEAKER_LEVEL;
                        /* Convert and Add data */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;
                    /* Vibrator level set */
                    case Component_Index.CoIdx_VibratorLevel:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_VIB_SET_VIBRATOR_LEVEL;
                        /* Convert and Add data */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;
                    /* Level set */
                    case Component_Index.CoIdx_SpeakerLevelPhase1:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_SPEAKER_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 1;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValueTimingSpeaker1.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        break;
                    case Component_Index.CoIdx_SpeakerLevelPhase2:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_SPEAKER_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 2;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValueTimingSpeaker2.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        break;
                    case Component_Index.CoIdx_SpeakerLevelPhase3:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_SPEAKER_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 3;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValueTimingSpeaker3.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        break;
                    case Component_Index.CoIdx_SpeakerLevelPhase4:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_SPEAKER_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 4;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValueTimingSpeaker4.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        break;
                    case Component_Index.CoIdx_SpeakerLevelPhase5:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_SPEAKER_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 5;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValueTimingSpeaker5.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        break;

                    /* Timing set */
                    case Component_Index.CoIdx_SpeakerTimingPhase1:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_SPEAKER_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 1;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValuelLevelSpeaker1.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;
                    case Component_Index.CoIdx_SpeakerTimingPhase2:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_SPEAKER_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 2;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValuelLevelSpeaker2.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;
                    case Component_Index.CoIdx_SpeakerTimingPhase3:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_SPEAKER_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 3;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValuelLevelSpeaker3.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;
                    case Component_Index.CoIdx_SpeakerTimingPhase4:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_SPEAKER_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 4;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValuelLevelSpeaker4.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;
                    case Component_Index.CoIdx_SpeakerTimingPhase5:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_SPEAKER_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 5;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValuelLevelSpeaker5.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;

                    /* Vibrator level set */
                    case Component_Index.CoIdx_VibratorLevelPhase1:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_VIBRATOR_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 1;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValueTimingVibrator1.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        break;
                    case Component_Index.CoIdx_VibratorLevelPhase2:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_VIBRATOR_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 2;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValueTimingVibrator2.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        break;
                    case Component_Index.CoIdx_VibratorLevelPhase3:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_VIBRATOR_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 3;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValueTimingVibrator3.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        break;
                    case Component_Index.CoIdx_VibratorLevelPhase4:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_VIBRATOR_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 4;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValueTimingVibrator4.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        break;
                    case Component_Index.CoIdx_VibratorLevelPhase5:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_VIBRATOR_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 5;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValueTimingVibrator5.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        break;

                    /* Timing set */
                    case Component_Index.CoIdx_VibratorTimingPhase1:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_VIBRATOR_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 1;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValuelLevelVibrator1.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;
                    case Component_Index.CoIdx_VibratorTimingPhase2:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_VIBRATOR_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 1;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValuelLevelVibrator2.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;
                    case Component_Index.CoIdx_VibratorTimingPhase3:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_VIBRATOR_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 1;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValuelLevelVibrator3.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;
                    case Component_Index.CoIdx_VibratorTimingPhase4:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_VIBRATOR_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 1;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValuelLevelVibrator4.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;
                    case Component_Index.CoIdx_VibratorTimingPhase5:
                        /* Add command */
                        pPackageFrame[iUART_CMD] = (byte)Cmd_Type.P2TCMD_SET_VIBRATOR_PHASE_X;
                        /* Add phase */
                        pPackageFrame[iUART_DATA] = 1;
                        /* Convert and Add level */
                        pValueString = Encoding.UTF8.GetString(pDataContent, 0, pDataContent.Length);
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 2] = (byte)value;
                        /* Convert and Add Timing */
                        pValueString = txtValuelLevelVibrator5.Text;
                        value = Convert.ToInt32(pValueString);
                        pPackageFrame[iUART_DATA + 1] = (byte)value;
                        break;
                    default:
                        break;
                }
            }
            catch { };
        }

        private string GetUserInputData()//(ref byte eDataField)
        {
            LayoutInflater layoutInflater = LayoutInflater.From(this);
            View view = layoutInflater.Inflate(Resource.Layout.user_input_dialog_box, null);
            Android.Support.V7.App.AlertDialog.Builder alertbuilder = new Android.Support.V7.App.AlertDialog.Builder(this);
            alertbuilder.SetView(view);
            var userdata = view.FindViewById<EditText>(Resource.Id.editText);
            var userInputTitle = view.FindViewById<TextView>(Resource.Id.dialogTitle);
            /* Parse and change display */
            switch (pComIndex)
            {
                case Component_Index.CoIdx_ProductID:
                        userInputTitle.Text = "Input Product ID";
                        userdata.Text = txtProductIDValue.Text;
                    break;
                case Component_Index.CoIdx_ProductActiveCode:
                        userInputTitle.Text = "Input Product Active code";
                        userdata.Text = txtProductActiveValue.Text;
                    break;
                case Component_Index.CoIdx_UserName:
                        userInputTitle.Text = "Input User Name";
                        userdata.Text = txtUserNameValue.Text;
                    break;
                case Component_Index.CoIdx_UserID:
                        userInputTitle.Text = "Input User ID";
                        userdata.Text = txtUserIDValue.Text;
                    break;
                case Component_Index.CoIdx_UserPass:
                        userInputTitle.Text = "Input User Pass";
                        userdata.Text = txtUserAccPassValue.Text;
                    break;
                case Component_Index.CoIdx_UserMail:
                        userInputTitle.Text = "Input User Mail";
                        userdata.Text = txtUserMailValue.Text;
                    break;
                case Component_Index.CoIdx_UserTel:
                        userInputTitle.Text = "Input User Telephone number";
                        userdata.Text = txtUserTelNumValue.Text;
                    break;
                case Component_Index.CoIdx_TimeUsing:
                    userInputTitle.Text = "Input Level Time Using";
                    userdata.Text = txtValuelTimeUsing.Text;
                    break;
                case Component_Index.CoIdx_SpeakerLevel:
                    userInputTitle.Text = "Input Level Speaker";
                    userdata.Text = txtValueSpeakerLevel.Text;
                    break;
                case Component_Index.CoIdx_VibratorLevel:
                    userInputTitle.Text = "Input Level Vibrator";
                    userdata.Text = txtValueVibratorLevel.Text;
                    break;
                case Component_Index.CoIdx_SpeakerLevelPhase1:
                        userInputTitle.Text = "Input Level Speaker phase 1";
                        userdata.Text = txtValuelLevelSpeaker1.Text;
                    break;
                case Component_Index.CoIdx_SpeakerLevelPhase2:
                        userInputTitle.Text = "Input Level Speaker phase 2";
                        userdata.Text = txtValuelLevelSpeaker2.Text;
                    break;
                case Component_Index.CoIdx_SpeakerLevelPhase3:
                        userInputTitle.Text = "Input Level Speaker phase 3";
                        userdata.Text = txtValuelLevelSpeaker3.Text;
                    break;
                case Component_Index.CoIdx_SpeakerLevelPhase4:
                        userInputTitle.Text = "Input Level Speaker phase 4";
                        userdata.Text = txtValuelLevelSpeaker4.Text;
                    break;
                case Component_Index.CoIdx_SpeakerLevelPhase5:
                        userInputTitle.Text = "Input Level Speaker phase 5";
                        userdata.Text = txtValuelLevelSpeaker5.Text;
                    break;
                case Component_Index.CoIdx_SpeakerTimingPhase1:
                        userInputTitle.Text = "Input Timing Speaker phase 1";
                        userdata.Text = txtValueTimingSpeaker1.Text;
                    break;
                case Component_Index.CoIdx_SpeakerTimingPhase2:
                        userInputTitle.Text = "Input Timing Speaker phase 2";
                        userdata.Text = txtValueTimingSpeaker2.Text;
                    break;
                case Component_Index.CoIdx_SpeakerTimingPhase3:
                        userInputTitle.Text = "Input Timing Speaker phase 3";
                        userdata.Text = txtValueTimingSpeaker3.Text;
                    break;
                case Component_Index.CoIdx_SpeakerTimingPhase4:
                        userInputTitle.Text = "Input Timing Speaker phase 4";
                        userdata.Text = txtValueTimingSpeaker4.Text;
                    break;
                case Component_Index.CoIdx_SpeakerTimingPhase5:
                        userInputTitle.Text = "Input Timing Speaker phase 5";
                        userdata.Text = txtValueTimingSpeaker5.Text;
                    break;

                case Component_Index.CoIdx_VibratorLevelPhase1:
                    userInputTitle.Text = "Input Level Vibrator phase 1";
                    userdata.Text = txtValuelLevelVibrator1.Text;
                    break;
                case Component_Index.CoIdx_VibratorLevelPhase2:
                    userInputTitle.Text = "Input Level Vibrator phase 2";
                    userdata.Text = txtValuelLevelVibrator2.Text;
                    break;
                case Component_Index.CoIdx_VibratorLevelPhase3:
                    userInputTitle.Text = "Input Level Vibrator phase 3";
                    userdata.Text = txtValuelLevelVibrator3.Text;
                    break;
                case Component_Index.CoIdx_VibratorLevelPhase4:
                    userInputTitle.Text = "Input Level Vibrator phase 4";
                    userdata.Text = txtValuelLevelVibrator4.Text;
                    break;
                case Component_Index.CoIdx_VibratorLevelPhase5:
                    userInputTitle.Text = "Input Level Vibrator phase 5";
                    userdata.Text = txtValuelLevelVibrator5.Text;
                    break;
                case Component_Index.CoIdx_VibratorTimingPhase1:
                    userInputTitle.Text = "Input Timing Vibrator phase 1";
                    userdata.Text = txtValueTimingVibrator1.Text;
                    break;
                case Component_Index.CoIdx_VibratorTimingPhase2:
                    userInputTitle.Text = "Input Timing Vibrator phase 2";
                    userdata.Text = txtValueTimingVibrator2.Text;
                    break;
                case Component_Index.CoIdx_VibratorTimingPhase3:
                    userInputTitle.Text = "Input Timing Vibrator phase 3";
                    userdata.Text = txtValueTimingVibrator3.Text;
                    break;
                case Component_Index.CoIdx_VibratorTimingPhase4:
                    userInputTitle.Text = "Input Timing Vibrator phase 4";
                    userdata.Text = txtValueTimingVibrator4.Text;
                    break;
                case Component_Index.CoIdx_VibratorTimingPhase5:
                    userInputTitle.Text = "Input Timing Vibrator phase 5";
                    userdata.Text = txtValueTimingVibrator5.Text;
                    break;
                default:
                    break;
            }
            /**/
            alertbuilder.SetCancelable(false)
            .SetPositiveButton("Submit", delegate
            {
                Toast.MakeText(this, "Submit Input: " + userdata.Text, ToastLength.Short).Show();
                bFlagGetUserDataInput = true;
                /* Copy data */
                StrDataInput = userdata.Text;
            })
            .SetNegativeButton("Cancel", delegate
            {
                alertbuilder.Dispose();
            });
            Android.Support.V7.App.AlertDialog dialog = alertbuilder.Create();
            dialog.Show();
            /* Return data */
            return StrDataInput;
        }

        /* Send and Display Package sent */
        private void SendAndDisplayLastPackageSent(ref byte[] pLastpackage)
        {
            Stream outStream = _bluetoothSocket.OutputStream;
            /* Send out data */
            outStream.WriteAsync(pLastpackage, 0, pLastpackage.Length);
            /* Check flag response already get */
            if (bFlagSendData == false)
            {
                /* Set flag send data */
                bFlagSendData = true;
                uCounterTimer = 0;
                /* Copy to last package sent */
                Array.Copy(pLastpackage, 0, PackageSent, 0, pLastpackage.Length);
                /* Copy to last package sent */
                var hexS = BitConverter.ToString(PackageSent, 0, pLastpackage.Length);
                /* Compare last package to change color */
                bool equalAB = tvPackageSent.Text.SequenceEqual(hexS);
                if (equalAB == true)
                {
                    /* Set new color */
                    if (tvPackageSent.CurrentTextColor == Android.Graphics.Color.Black)
                    {
                        tvPackageSent.SetTextColor(Android.Graphics.Color.Red);
                    }
                    else
                    {
                        tvPackageSent.SetTextColor(Android.Graphics.Color.Black);
                    }
                }
                else
                {
                    /* Set color default */
                    tvPackageSent.SetTextColor(Android.Graphics.Color.Black);
                }
                /* Display */
                tvPackageSent.Text = hexS.ToString();
            }
        }
        private PlotModel CreatePlotModelSpeaker()
        {
            var plotModel = new PlotModel { Title = "User Profile Speaker" };
            plotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom });
            plotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Maximum = 10, Minimum = 0 });

            /* Init Line Speaker */
            var seriesSpeaker = new LineSeries
            {
                MarkerType = MarkerType.Circle,
                MarkerSize = 4,
                MarkerStroke = OxyColors.Black
            };

            /* Clear data */
            seriesSpeaker.Points.Clear();
            /* Add Speaker point */
            var uTiming = Convert.ToDouble(txtValuelTimeUsing.Text);
            seriesSpeaker.Points.Add(new DataPoint(0.0,0.0));
            seriesSpeaker.Points.Add(new DataPoint(Convert.ToDouble(txtValueTimingSpeaker1.Text) * uTiming / 100, Convert.ToDouble(txtValuelLevelSpeaker1.Text)));
            seriesSpeaker.Points.Add(new DataPoint(Convert.ToDouble(txtValueTimingSpeaker2.Text) * uTiming / 100, Convert.ToDouble(txtValuelLevelSpeaker2.Text)));
            seriesSpeaker.Points.Add(new DataPoint(Convert.ToDouble(txtValueTimingSpeaker3.Text) * uTiming / 100, Convert.ToDouble(txtValuelLevelSpeaker3.Text)));
            seriesSpeaker.Points.Add(new DataPoint(Convert.ToDouble(txtValueTimingSpeaker4.Text) * uTiming / 100, Convert.ToDouble(txtValuelLevelSpeaker4.Text)));
            seriesSpeaker.Points.Add(new DataPoint(Convert.ToDouble(txtValueTimingSpeaker5.Text) * uTiming / 100, Convert.ToDouble(txtValuelLevelSpeaker5.Text)));
            seriesSpeaker.Points.Add(new DataPoint(Convert.ToDouble(txtValuelTimeUsing.Text), 0));
            plotModel.Series.Add(seriesSpeaker);

            return plotModel;
        }
        private PlotModel CreatePlotModelVibrator()
        {
            var plotModel = new PlotModel { Title = "User Profile Vibrator" };
            plotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom });
            plotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Maximum = 10, Minimum = 0 });

            /* Init Vibrator */
            var seriesSVibrator = new LineSeries
            {
                MarkerType = MarkerType.Circle,
                MarkerSize = 4,
                MarkerStroke = OxyColors.Black
            };

            /* Clear data */
            seriesSVibrator.Points.Clear();
            /* Add TP9 */
            var uTiming = Convert.ToDouble(txtValuelTimeUsing.Text);
            seriesSVibrator.Points.Add(new DataPoint(0.0, 0.0));
            seriesSVibrator.Points.Add(new DataPoint(Convert.ToDouble(txtValueTimingVibrator1.Text) * uTiming / 100, Convert.ToDouble(txtValuelLevelVibrator1.Text)));
            seriesSVibrator.Points.Add(new DataPoint(Convert.ToDouble(txtValueTimingVibrator2.Text) * uTiming / 100, Convert.ToDouble(txtValuelLevelVibrator2.Text))); 
            seriesSVibrator.Points.Add(new DataPoint(Convert.ToDouble(txtValueTimingVibrator3.Text) * uTiming / 100, Convert.ToDouble(txtValuelLevelVibrator3.Text)));
            seriesSVibrator.Points.Add(new DataPoint(Convert.ToDouble(txtValueTimingVibrator4.Text) * uTiming / 100, Convert.ToDouble(txtValuelLevelVibrator4.Text)));
            seriesSVibrator.Points.Add(new DataPoint(Convert.ToDouble(txtValueTimingVibrator5.Text) * uTiming / 100, Convert.ToDouble(txtValuelLevelVibrator5.Text)));
            seriesSVibrator.Points.Add(new DataPoint(Convert.ToDouble(txtValuelTimeUsing.Text), 0));
            plotModel.Series.Add(seriesSVibrator);

            return plotModel;
        }
        public async Task Connect(CancellationToken? cancellationToken)
        {
            listener();
        }
        public async Task UpdateUI()
        {
            await Task.Run(() =>
            {
                try
                {
                    while (true)
                    {
                        Thread.Sleep(1);
                        /* Refresh UI */
                        bRefreshUI++;
                        if (bRefreshUI >= 10)
                        {
                            bRefreshUI = 0;
                            RunOnUiThread(() =>
                            {
                                /* Check flag and update value of UI component */
                                if (bFlagGetUserDataInput == true)
                                {
                                    bFlagGetUserDataInput = false;
                                    /* Parse component */
                                    switch (pComIndex)
                                    {
                                        case Component_Index.CoIdx_ProductID:
                                            /* Update data */
                                            txtProductIDValue.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtProductIDValue.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtProductCmd.Text = "Set";
                                            txtProductCmd.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_ProductActiveCode:
                                            /* Update data */
                                            txtProductActiveValue.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtProductActiveValue.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtActiveCmd.Text = "Set";
                                            txtActiveCmd.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_UserName:
                                            /* Update data */
                                            txtUserNameValue.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtUserNameValue.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtUserNameCmd.Text = "Set";
                                            txtUserNameCmd.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_UserID:
                                            /* Update data */
                                            txtUserIDValue.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtUserIDValue.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtUserIDCmd.Text = "Set";
                                            txtUserIDCmd.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_UserPass:
                                            /* Update data */
                                            txtUserAccPassValue.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtUserAccPassValue.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtUserAccPassCmd.Text = "Set";
                                            txtUserAccPassCmd.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_UserTel:
                                            /* Update data */
                                            txtUserTelNumValue.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtUserTelNumValue.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtUserTelNumCmd.Text = "Set";
                                            txtUserTelNumCmd.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_UserMail:
                                            /* Update data */
                                            txtUserMailValue.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtUserMailValue.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtUserMailCmd.Text = "Set";
                                            txtUserMailCmd.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_TimeUsing:
                                            /* Update data */
                                            txtValuelTimeUsing.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValuelTimeUsing.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetTimeUsing.Text = "Set";
                                            txtGetTimeUsing.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_SpeakerLevel:
                                            /* Update data */
                                            txtValueSpeakerLevel.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValueSpeakerLevel.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetSpeakerLevel.Text = "Set";
                                            txtGetSpeakerLevel.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_VibratorLevel:
                                            /* Update data */
                                            txtValueVibratorLevel.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValueVibratorLevel.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetVibratorLevel.Text = "Set";
                                            txtGetVibratorLevel.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_SpeakerLevelPhase1:
                                            /* Update data */
                                            txtValuelLevelSpeaker1.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValuelLevelSpeaker1.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetSpeaker1.Text = "Set";
                                            txtGetSpeaker1.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_SpeakerLevelPhase2:
                                            /* Update data */
                                            txtValuelLevelSpeaker2.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValuelLevelSpeaker2.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetSpeaker2.Text = "Set";
                                            txtGetSpeaker2.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_SpeakerLevelPhase3:
                                            /* Update data */
                                            txtValuelLevelSpeaker3.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValuelLevelSpeaker3.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetSpeaker3.Text = "Set";
                                            txtGetSpeaker3.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_SpeakerLevelPhase4:
                                            /* Update data */
                                            txtValuelLevelSpeaker4.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValuelLevelSpeaker4.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetSpeaker4.Text = "Set";
                                            txtGetSpeaker4.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_SpeakerLevelPhase5:
                                            /* Update data */
                                            txtValuelLevelSpeaker5.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValuelLevelSpeaker5.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetSpeaker5.Text = "Set";
                                            txtGetSpeaker5.SetTextColor(Android.Graphics.Color.Red);
                                            break;

                                        case Component_Index.CoIdx_SpeakerTimingPhase1:
                                            /* Update data */
                                            txtValueTimingSpeaker1.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValueTimingSpeaker1.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetSpeaker1.Text = "Set";
                                            txtGetSpeaker1.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_SpeakerTimingPhase2:
                                            /* Update data */
                                            txtValueTimingSpeaker2.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValueTimingSpeaker2.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetSpeaker2.Text = "Set";
                                            txtGetSpeaker2.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_SpeakerTimingPhase3:
                                            /* Update data */
                                            txtValueTimingSpeaker3.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValueTimingSpeaker3.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetSpeaker3.Text = "Set";
                                            txtGetSpeaker3.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_SpeakerTimingPhase4:
                                            /* Update data */
                                            txtValueTimingSpeaker4.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValueTimingSpeaker4.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetSpeaker4.Text = "Set";
                                            txtGetSpeaker4.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_SpeakerTimingPhase5:
                                            /* Update data */
                                            txtValueTimingSpeaker5.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValueTimingSpeaker5.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetSpeaker5.Text = "Set";
                                            txtGetSpeaker5.SetTextColor(Android.Graphics.Color.Red);
                                            break;

                                        case Component_Index.CoIdx_VibratorLevelPhase1:
                                            /* Update data */
                                            txtValuelLevelVibrator1.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValuelLevelVibrator1.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetVibrator1.Text = "Set";
                                            txtGetVibrator1.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_VibratorLevelPhase2:
                                            /* Update data */
                                            txtValuelLevelVibrator2.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValuelLevelVibrator2.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetVibrator2.Text = "Set";
                                            txtGetVibrator2.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_VibratorLevelPhase3:
                                            /* Update data */
                                            txtValuelLevelVibrator3.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValuelLevelVibrator3.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetVibrator3.Text = "Set";
                                            txtGetVibrator3.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_VibratorLevelPhase4:
                                            /* Update data */
                                            txtValuelLevelVibrator4.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValuelLevelVibrator4.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetVibrator4.Text = "Set";
                                            txtGetVibrator4.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_VibratorLevelPhase5:
                                            /* Update data */
                                            txtValuelLevelVibrator5.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValuelLevelVibrator5.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetVibrator5.Text = "Set";
                                            txtGetVibrator5.SetTextColor(Android.Graphics.Color.Red);
                                            break;

                                        case Component_Index.CoIdx_VibratorTimingPhase1:
                                            /* Update data */
                                            txtValueTimingVibrator1.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValueTimingVibrator1.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetVibrator1.Text = "Set";
                                            txtGetVibrator1.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_VibratorTimingPhase2:
                                            /* Update data */
                                            txtValueTimingVibrator2.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValueTimingVibrator2.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetVibrator2.Text = "Set";
                                            txtGetVibrator2.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_VibratorTimingPhase3:
                                            /* Update data */
                                            txtValueTimingVibrator3.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValueTimingVibrator3.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetVibrator3.Text = "Set";
                                            txtGetVibrator3.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_VibratorTimingPhase4:
                                            /* Update data */
                                            txtValueTimingVibrator4.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValueTimingVibrator4.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetVibrator4.Text = "Set";
                                            txtGetVibrator4.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        case Component_Index.CoIdx_VibratorTimingPhase5:
                                            /* Update data */
                                            txtValueTimingVibrator5.Text = StrDataInput;
                                            /* Change color, ready to send */
                                            txtValueTimingVibrator5.SetTextColor(Android.Graphics.Color.Red);
                                            /* Change content and color */
                                            txtGetVibrator5.Text = "Set";
                                            txtGetVibrator5.SetTextColor(Android.Graphics.Color.Red);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                bRefreshGraph++;
                                if (bRefreshGraph >= 10)
                                {
                                    bRefreshGraph = 0;
                                    /* Refresh every 100ms */
                                    Speakerview.Model = CreatePlotModelSpeaker();
                                    Vibratorview.Model = CreatePlotModelVibrator();
                                }
                            });
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            });
        }

        void parserData(ref byte[] pPackage)
        {
            /* Process Ringbuf amd [arser data of Ringbuf here */


            /* Data parser */
            byte[] bDataFilter = new byte[4096];
            /* Position command Index in frame */
            Cmd_Type bCommandID = (Cmd_Type)pPackage[iUART_CMD];
            
            /* Check command ID */
            switch(bCommandID)
            {
                /* Product ID command */
                case Cmd_Type.P2TCMD_GET_PRODUCT_ID:
                    /* Get product ID */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, PRODUCT_ID_SIZE);
                    var hexS = BitConverter.ToString(bDataFilter, 0, PRODUCT_ID_SIZE);
                    hexS = System.Text.Encoding.UTF8.GetString(bDataFilter, 0, PRODUCT_ID_SIZE);
                    /* Display product ID */
                    txtProductIDValue.Text = hexS;
                    txtProductIDValue.SetTextColor(Android.Graphics.Color.Blue);
                    break;
                case Cmd_Type.P2TCMD_GET_PRODUCT_ACTIVE:
                    /* Get product Active code */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, SIZE_ACTIVE_CODE);
                    hexS = BitConverter.ToString(bDataFilter, 0, SIZE_ACTIVE_CODE);
                    hexS = System.Text.Encoding.UTF8.GetString(bDataFilter, 0, SIZE_ACTIVE_CODE);
                    /* Display product ID */
                    txtProductActiveValue.Text = hexS;
                    txtProductActiveValue.SetTextColor(Android.Graphics.Color.Blue);
                    break;
                case Cmd_Type.P2TCMD_GET_HW_VER:
                    /* Get product ID */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, HW_VERSION_SIZE);
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, HW_VERSION_SIZE);
                    hexS = BitConverter.ToString(bDataFilter, 0, HW_VERSION_SIZE);
                    hexS = System.Text.Encoding.UTF8.GetString(bDataFilter, 0, HW_VERSION_SIZE);
                    /* Display product ID */
                    txtProductHWValue.Text = hexS;
                    txtProductHWValue.SetTextColor(Android.Graphics.Color.Blue);
                    break;
                case Cmd_Type.P2TCMD_GET_FW_VER:
                    /* Get product ID */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, FW_VERSION_SIZE);
                    hexS = BitConverter.ToString(bDataFilter, 0, FW_VERSION_SIZE);
                    hexS = System.Text.Encoding.UTF8.GetString(bDataFilter, 0, FW_VERSION_SIZE);
                    /* Display product ID */
                    txtProductFWValue.Text = hexS;
                    txtProductFWValue.SetTextColor(Android.Graphics.Color.Blue);
                    break;
                case Cmd_Type.P2TCMD_GET_BL_VER:
                    /* Get product ID */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, BL_VERSION_SIZE);
                    hexS = BitConverter.ToString(bDataFilter, 0, BL_VERSION_SIZE);
                    hexS = System.Text.Encoding.UTF8.GetString(bDataFilter, 0, BL_VERSION_SIZE);
                    /* Display product ID */
                    txtProductBLValue.Text = hexS;
                    txtProductBLValue.SetTextColor(Android.Graphics.Color.Blue);
                    break;
                case Cmd_Type.P2TCMD_GET_USER_NAME:
                    /* Get product ID */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, SIZE_USER_NAME);
                    hexS = BitConverter.ToString(bDataFilter, 0, SIZE_USER_NAME);
                    hexS = System.Text.Encoding.UTF8.GetString(bDataFilter, 0, SIZE_USER_NAME);
                    /* Display product ID */
                    txtUserNameValue.Text = hexS;
                    txtUserNameValue.SetTextColor(Android.Graphics.Color.Blue);
                    break;
                case Cmd_Type.P2TCMD_GET_USER_ID:
                    /* Get product ID */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, SIZE_ID_ACCOUNT);
                    hexS = BitConverter.ToString(bDataFilter, 0, SIZE_ID_ACCOUNT);
                    hexS = System.Text.Encoding.UTF8.GetString(bDataFilter, 0, SIZE_ID_ACCOUNT);
                    /* Display product ID */
                    txtUserIDValue.Text = hexS;
                    txtUserIDValue.SetTextColor(Android.Graphics.Color.Blue);
                    break;
                case Cmd_Type.P2TCMD_GET_USER_TELE_NUM:
                    /* Get product ID */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, SIZE_USER_TELEPHONE);
                    hexS = BitConverter.ToString(bDataFilter, 0, SIZE_USER_TELEPHONE);
                    hexS = System.Text.Encoding.UTF8.GetString(bDataFilter, 0, SIZE_USER_TELEPHONE);
                    /* Display product ID */
                    txtUserTelNumValue.Text = hexS;
                    txtUserTelNumValue.SetTextColor(Android.Graphics.Color.Blue);
                    break;
                case Cmd_Type.P2TCMD_GET_USER_MAIL:
                    /* Get product ID */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, SIZE_USER_MAIL);
                    hexS = BitConverter.ToString(bDataFilter, 0, SIZE_USER_MAIL);
                    hexS = System.Text.Encoding.UTF8.GetString(bDataFilter, 0, SIZE_USER_MAIL);
                    /* Display product ID */
                    txtUserMailValue.Text = hexS;
                    txtUserMailValue.SetTextColor(Android.Graphics.Color.Blue);
                    break;
                case Cmd_Type.P2TCMD_GET_USER_ACCPASS:
                    /* Get product ID */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, SIZE_ACCOUNT_PASSWORD);
                    hexS = BitConverter.ToString(bDataFilter, 0, SIZE_ACCOUNT_PASSWORD);
                    hexS = System.Text.Encoding.UTF8.GetString(bDataFilter, 0, SIZE_ACCOUNT_PASSWORD);
                    /* Display product ID */
                    txtUserAccPassValue.Text = hexS;
                    txtUserAccPassValue.SetTextColor(Android.Graphics.Color.Blue);
                    break;
                case Cmd_Type.P2TCMD_GET_TIME_USING:
                    /* Get Time Using */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, 1);
                    hexS = BitConverter.ToString(bDataFilter, 0, 1);
                    int num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                    /* Display Time using */
                    txtValuelTimeUsing.Text = num.ToString();
                    txtValuelTimeUsing.SetTextColor(Android.Graphics.Color.Blue);
                    break;

                case Cmd_Type.P2TCMD_MEDIA_GET_SPEAKER_LEVEL:
                    /* Get Speaker level */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, 1);
                    hexS = BitConverter.ToString(bDataFilter, 0, 1);
                    num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                    /* Display Time using */
                    txtValueSpeakerLevel.Text = num.ToString();
                    txtValueSpeakerLevel.SetTextColor(Android.Graphics.Color.Blue);
                    break;
                case Cmd_Type.P2TCMD_VIB_GET_VIBRATOR_LEVEL:
                    /* Get Vibrator level */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, 1);
                    hexS = BitConverter.ToString(bDataFilter, 0, 1);
                    num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                    /* Display Time using */
                    txtValueVibratorLevel.Text = num.ToString();
                    txtValueVibratorLevel.SetTextColor(Android.Graphics.Color.Blue);
                    break;
                case Cmd_Type.P2TCMD_SONG_SELECTION:
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, 3);
                    hexS = BitConverter.ToString(bDataFilter, 1, 1);
                    num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                    var hexS1 = BitConverter.ToString(bDataFilter, 2, 1);
                    var num1 = Int32.Parse(hexS1, System.Globalization.NumberStyles.HexNumber);
                    var numSum = num * 256 + num1;
                    switch (bDataFilter[0])
                    {
                        case 1:
                            btnSong1.Text = "1:" + numSum.ToString();
                            break;
                        case 2:
                            btnSong2.Text = "2:" + numSum.ToString();
                            break;
                        case 3:
                            btnSong3.Text = "3:" + numSum.ToString();
                            break;
                        case 4:
                            btnSong4.Text = "4:" + numSum.ToString();
                            break;
                        case 5:
                            btnSong5.Text = "5:" + numSum.ToString();
                            break;
                        case 6:
                            btnSong6.Text = "6:" + numSum.ToString();
                            break;
                    }
                    break;
                case Cmd_Type.P2TCMD_GET_A2DP_STATE:
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, 3);
                    switch ((char)bDataFilter[0])
                    {
                        case '0':
                            txtA2DPState.Text = "Unsupported!";
                            break;
                        case '1':
                            txtA2DPState.Text = "Standby!";
                            break;
                        case '2':
                            txtA2DPState.Text = "Connecting!";
                            break;
                        case '3':
                            txtA2DPState.Text = "Connected!";
                            break;
                        case '4':
                            txtA2DPState.Text = "MediaStreaming!";
                            break;
                        case '5':
                            txtA2DPState.Text = "MediaPaused!";
                            break;
                    }
                    break;
                case Cmd_Type.P2TCMD_GET_SPEAKER_PHASE_X:
                    /* Get Phase X speaker data */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, SIZE_PHASE+1);
                    /* Display phase data */
                    switch(bDataFilter[0])
                    {
                        case 1:
                            hexS = BitConverter.ToString(bDataFilter, 1, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValuelLevelSpeaker1.Text = num.ToString();
                            txtValuelLevelSpeaker1.SetTextColor(Android.Graphics.Color.Blue);
                            hexS = BitConverter.ToString(bDataFilter, 2, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValueTimingSpeaker1.Text = num.ToString();
                            txtValueTimingSpeaker1.SetTextColor(Android.Graphics.Color.Blue);
                            break;
                        case 2:
                            hexS = BitConverter.ToString(bDataFilter, 1, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValuelLevelSpeaker2.Text = num.ToString();
                            txtValuelLevelSpeaker2.SetTextColor(Android.Graphics.Color.Blue);
                            hexS = BitConverter.ToString(bDataFilter, 2, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValueTimingSpeaker2.Text = num.ToString();
                            txtValueTimingSpeaker2.SetTextColor(Android.Graphics.Color.Blue);
                            break;
                        case 3:
                            hexS = BitConverter.ToString(bDataFilter, 1, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValuelLevelSpeaker3.Text = num.ToString();
                            txtValuelLevelSpeaker3.SetTextColor(Android.Graphics.Color.Blue);
                            hexS = BitConverter.ToString(bDataFilter, 2, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValueTimingSpeaker3.Text = num.ToString();
                            txtValueTimingSpeaker3.SetTextColor(Android.Graphics.Color.Blue);
                            break;
                        case 4:
                            hexS = BitConverter.ToString(bDataFilter, 1, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValuelLevelSpeaker4.Text = num.ToString();
                            txtValuelLevelSpeaker4.SetTextColor(Android.Graphics.Color.Blue);
                            hexS = BitConverter.ToString(bDataFilter, 2, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValueTimingSpeaker4.Text = num.ToString();
                            txtValueTimingSpeaker4.SetTextColor(Android.Graphics.Color.Blue);
                            break;
                        case 5:
                            hexS = BitConverter.ToString(bDataFilter, 1, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValuelLevelSpeaker5.Text = num.ToString();
                            txtValuelLevelSpeaker5.SetTextColor(Android.Graphics.Color.Blue);
                            hexS = BitConverter.ToString(bDataFilter, 2, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValueTimingSpeaker5.Text = num.ToString();
                            txtValueTimingSpeaker5.SetTextColor(Android.Graphics.Color.Blue);
                            break;
                    }
                    break;
                case Cmd_Type.P2TCMD_SET_SPEAKER_PHASE_X:
                    break;
                case Cmd_Type.P2TCMD_GET_VIBRATOR_PHASE_X:
                    /* Get Phase X vibrator data */
                    /* Copy to last package sent */
                    Array.Copy(pPackage, iUART_DATA_PACKAGE, bDataFilter, 0, SIZE_PHASE + 1);
                    /* Display phase data */
                    switch (bDataFilter[0])
                    {
                        case 1:
                            hexS = BitConverter.ToString(bDataFilter, 1, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValuelLevelVibrator1.Text = num.ToString();
                            txtValuelLevelVibrator1.SetTextColor(Android.Graphics.Color.Blue);
                            hexS = BitConverter.ToString(bDataFilter, 2, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValueTimingVibrator1.Text = num.ToString();
                            txtValueTimingVibrator1.SetTextColor(Android.Graphics.Color.Blue);
                            break;
                        case 2:
                            hexS = BitConverter.ToString(bDataFilter, 1, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValuelLevelVibrator2.Text = num.ToString();
                            txtValuelLevelVibrator2.SetTextColor(Android.Graphics.Color.Blue);
                            hexS = BitConverter.ToString(bDataFilter, 2, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValueTimingVibrator2.Text = num.ToString();
                            txtValueTimingVibrator2.SetTextColor(Android.Graphics.Color.Blue);
                            break;
                        case 3:
                            hexS = BitConverter.ToString(bDataFilter, 1, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValuelLevelVibrator3.Text = num.ToString();
                            txtValuelLevelVibrator3.SetTextColor(Android.Graphics.Color.Blue);
                            hexS = BitConverter.ToString(bDataFilter, 2, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValueTimingVibrator3.Text = num.ToString();
                            txtValueTimingVibrator3.SetTextColor(Android.Graphics.Color.Blue);
                            break;
                        case 4:
                            hexS = BitConverter.ToString(bDataFilter, 1, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValuelLevelVibrator4.Text = num.ToString();
                            txtValuelLevelVibrator4.SetTextColor(Android.Graphics.Color.Blue);
                            hexS = BitConverter.ToString(bDataFilter, 2, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValueTimingVibrator4.Text = num.ToString();
                            txtValueTimingVibrator4.SetTextColor(Android.Graphics.Color.Blue);
                            break;
                        case 5:
                            hexS = BitConverter.ToString(bDataFilter, 1, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValuelLevelVibrator5.Text = num.ToString();
                            txtValuelLevelVibrator5.SetTextColor(Android.Graphics.Color.Blue);
                            hexS = BitConverter.ToString(bDataFilter, 2, 1);
                            num = Int32.Parse(hexS, System.Globalization.NumberStyles.HexNumber);
                            txtValueTimingVibrator5.Text = num.ToString();
                            txtValueTimingVibrator5.SetTextColor(Android.Graphics.Color.Blue);
                            break;
                    }
                    break;
                case Cmd_Type.P2TCMD_SET_VIBRATOR_PHASE_X:
                    break;
                default:
                break;
            }
        }
        void listener()
        {
            byte[] read = new byte[1];
            Task.Run(() =>
            {
                try
                {
                    /* For Init ringbuf */
                    var RingBuFTestBuffer = new CircularBuffer<byte>(4096);

                    /* Running task */
                    _disconnectTokenSource = new CancellationTokenSource();
                    _running = true;
                    while (_running && !_disconnectTokenSource.IsCancellationRequested)
                    {
                        /* Check flag running process */
                        if (bRunningProcess == true)
                        {
                            /* Check bluetooth socket connection */
                            if (_bluetoothSocket.IsConnected)
                            {
                                Stream inStream = _bluetoothSocket.InputStream;
                                /* Check data on buffer or not? */
                                /* Just get maximum 1000bytes per 10ms */
                                if (inStream.IsDataAvailable())
                                {
                                    _bluetoothSPPDataReceived = true;
                                    // get 1 newest byte every 10ms 
                                    //inStream.ReadAsync(ringBufReceivedData, 0, 1);
                                    /* Continue reading when buffer under 1kB or end of package happen */
                                    while ((inStream.IsDataAvailable()) && (bNumberBytesPerPackageReceivedCounter < 1000))
                                    //while (inStream.IsDataAvailable())
                                    {
                                        /* Read single byte every 10ms */
                                        //int bDataReceived;
                                        //bDataReceived = inStream.ReadByte();
                                        //ringBufReceivedData[bNumberBytesPerPackageReceivedCounter] = (byte)bDataReceived;
                                        if (inStream.CanRead)
                                        {
                                            /* Read byte and put to Ringbuf */
                                            byte bDataReceived;
                                            bDataReceived = (byte)inStream.ReadByte();
                                            ringBufReceivedData[bNumberBytesPerPackageReceivedCounter] = bDataReceived;
                                            /* Increase numberbyte received */
                                            bNumberBytesPerPackageReceivedCounter++;

                                            /* Push to ringbuf */
                                            RingBuFTestBuffer.PushBack(bDataReceived);
                                            bFlagPackageReady = true;
                                        };
                                    }
                                }

                                /* Check timeout */
                                if ((bFlagIntervalTimeout == true)&&(bFlagPackageReady==true))
                                {
                                    bFlagPackageReady = false;
                                    /* Clear flag timeout */
                                    bFlagIntervalTimeout = false;
                                    bFlagSendData = false;
                                    /* Test read data from ringbuf */
                                    /* Get all data until reach bNumberBytesLastPackage */
                                    byte[] bReceiveByteRingbuf = new byte[4096];
                                    for (int i = 0; i< bNumberBytesPerPackageReceivedCounter; i++)
                                    {
                                        bReceiveByteRingbuf[i] = RingBuFTestBuffer.PopFront();
                                    }
                                    /* Display data */
                                    /* Copy to last package sent */
                                    Array.Copy(bReceiveByteRingbuf, 0, PackageReceived, 0, bNumberBytesPerPackageReceivedCounter);
                                    var hexS = BitConverter.ToString(PackageReceived, 0, bNumberBytesPerPackageReceivedCounter);

                                    /* Update UI */
                                    RunOnUiThread(() =>
                                    {
                                        /* Compare last package to change color */
                                        bool equalAB = tvPackageReceived.Text.SequenceEqual(hexS);
                                        if (equalAB == true)
                                        {
                                            if (tvPackageReceived.CurrentTextColor == Android.Graphics.Color.Black)
                                            {
                                                tvPackageReceived.SetTextColor(Android.Graphics.Color.Red);
                                            }
                                            else
                                            {
                                                tvPackageReceived.SetTextColor(Android.Graphics.Color.Black);
                                            }
                                        }
                                        else
                                        {
                                            /* Set color default */
                                            tvPackageReceived.SetTextColor(Android.Graphics.Color.Black);
                                        }
                                        /* Display */
                                        tvPackageReceived.Text = hexS.ToString();
                                        /* Parse data */
                                        parserData(ref PackageReceived);
                                    });

                                    /* Reset counter */
                                    bNumberBytesPerPackageReceivedCounter = 0;

                                    /* Clear all cache data */
                                    int BufferSize = RingBuFTestBuffer.Size;
                                    for (int i = 0; i < BufferSize; i++)
                                    {
                                        bReceiveByteRingbuf[i] = RingBuFTestBuffer.PopFront();
                                    }
                                }
                            }
                        }
                    };
                }
                catch (Exception ex)
                {
                    _bluetoothSocket?.Close();
                }
                finally
                {
                    _bluetoothSocket?.Close();
                }
            });
        }

        #region Private Fields
        private TextView NumberPackageReceived;
        private TextView NumberBytesReceived;
        private TextView NewestPackageReceived;
        private TextView NumberBytesLastPackageReceived;
        private TextView UIResponse;
        private TextView uTimeStart;
        private TextView uTimeEnd;
        private EditText uIntervalValue;
        private EditText uBytestPerPackageValue;
        private EditText StrInputDeviceName;
        private TextView tvTimeProcess;
        private TextView tvPackageReceived;
        private TextView tvPackageSent;
        private TextView tvNumberPackageReceivedExpect;
        private TextView tvNumberDataReceived;
        private TextView tvNumberDataReceivedExpect;
        private TextView tvNumberDataLastPackage;
        private TextView tvNumberPackageMissing;
        private TextView tvNumberBytesMissing;
        private TextView tvRateDataMissing;
        private TextView tvNumberPackageMissingFilter;
        private TextView tvNumberBytesMissingFilter;
        private TextView tvRateDataMissingFilter;
        private TextView tvSamplePackageMatchResult;
        private TextView tvFailMatchCounter;
        private TextView tvNumberPackageReceivedFilter;
        private TextView tvNumberDataReceivedFilter;
        private TextView tvDiffResult;
        private TextView tvIndexMissResult;
        private TextView tvDetectErrorResult;
        private TextView txtUserProfileCommandActive;
        private TextView txtUserProfileCommandDeactive;

        /* Media control*/
        private TextView txtPlayPauseCommand;
        private TextView txtGetSpeakerLevel, txtGetVibratorLevel;
        private TextView txtValueSpeakerLevel, txtValueVibratorLevel;

        /* Product Information */
        private TextView txtProductIDValue;
        private TextView txtProductHWValue;
        private TextView txtProductFWValue;
        private TextView txtProductBLValue;
        private TextView txtProductActiveValue;

        private TextView txtProductCmd;
        private TextView txtHWCmd;
        private TextView txtFWCmd;
        private TextView txtBLCmd;
        private TextView txtActiveCmd;

        /* User Information */
        private TextView txtUserNameValue;
        private TextView txtUserIDValue;
        private TextView txtUserAccPassValue;
        private TextView txtUserTelNumValue;
        private TextView txtUserMailValue;

        private TextView txtUserNameCmd;
        private TextView txtUserIDCmd;
        private TextView txtUserAccPassCmd;
        private TextView txtUserTelNumCmd;
        private TextView txtUserMailCmd;

        /* User Profile */
        private TextView txtGetSpeaker1;
        private TextView txtGetVibrator1;
        private TextView txtValuelLevelSpeaker1;
        private TextView txtValueTimingSpeaker1;
        private TextView txtValuelLevelVibrator1;
        private TextView txtValueTimingVibrator1;

        private TextView txtGetSpeaker2;
        private TextView txtGetVibrator2;
        private TextView txtValuelLevelSpeaker2;
        private TextView txtValueTimingSpeaker2;
        private TextView txtValuelLevelVibrator2;
        private TextView txtValueTimingVibrator2;

        private TextView txtGetSpeaker3;
        private TextView txtGetVibrator3;
        private TextView txtValuelLevelSpeaker3;
        private TextView txtValueTimingSpeaker3;
        private TextView txtValuelLevelVibrator3;
        private TextView txtValueTimingVibrator3;

        private TextView txtGetSpeaker4;
        private TextView txtGetVibrator4;
        private TextView txtValuelLevelSpeaker4;
        private TextView txtValueTimingSpeaker4;
        private TextView txtValuelLevelVibrator4;
        private TextView txtValueTimingVibrator4;

        private TextView txtGetSpeaker5;
        private TextView txtGetVibrator5;
        private TextView txtValuelLevelSpeaker5;
        private TextView txtValueTimingSpeaker5;
        private TextView txtValuelLevelVibrator5;
        private TextView txtValueTimingVibrator5;

        private TextView txtGetTimeUsing;
        private TextView txtValuelTimeUsing;
        private Button btnSong1, btnSong2, btnSong3, btnSong4, btnSong5, btnSong6;

        private TextView txtA2DPState;

        private int bInterval;
        private int bBytesPerPackage;
        private int bRefreshUI;
        private int bRefreshGraph;
        private DateTime bDateTimeStart;
        private DateTime bDateTimeEnd;
        private DateTime bDateTimeCurrent;
        /* Variable for connection process */
        private System.Timers.Timer bTimerQuerryReceiveData;
        private bool bRunningProcess = false;
        private bool bFlagGetTimeStart = false;
        private bool bRunningGet1stPackage = false;
        private TimeSpan bRunTime;
        private int bProcessedTime;
        /* Variable for calculate data process */
        private int bNumberPackageExpect;
        private int bNumberPackageMissing;
        private int bNumberDataExpect;
        private int bNumberDataMissing;
        private float fNumberRateDataMissing;
        private BluetoothDevice _bluetoothDevice;
        private BluetoothSocket _bluetoothSocket;
        private bool _running;
        private bool _bluetoothSPPDataReceived = false;
        private int _bluetoothSPPGetFullPackageData = 0;
        private int bNumberPackageReceivedCounter = 0;
        private int bNumberPackageReceivedCounterFilter = 0;
        private int bNumberBytesReceivedCounter = 0;
        private int bNumberUnMatchPackage = 0;
        private int bNumberBytesPerPackageReceivedCounter = 0;
        private int bNumberBytesLastPackage = 0;
        private int bNumberBytesLastDiffCounter = 0;
        private int bNumberBytesReceivedCounterFilter = 0;
        private int bDetectErrorFail = 0;
        byte[] SamplePackage;
        private string pathLogger;
        private string filenameLogger;
        private CancellationTokenSource _disconnectTokenSource;
        private CancellationTokenSource _cancellationSource;
        private int _lastDataLen = 0;
        private byte[] ringBufReceivedData = new byte[4096];
        private byte[] cacheBufReceiveData = new byte[4096];
        private byte[] PackageSent = new byte[2048];
        private byte[] PackageReceived = new byte[4096];
        private string strPackageSample = "---0123456789 We start test thoughput mode perfomance missing package happen or not? 9876543210---00";

        /* EEG data and ACCE data */
        private TextView tvTP9, tvFP1, tvFP2, tvTP10;
        private TextView tvACCE_X, tvACCE_Y, tvACCE_Z;
        private TextView tvGYRO_X, tvGYRO_Y, tvGYRO_Z;

        /* Command protocol */
        /* Connect/Disconnect command */
        private byte[] ConnectProtocol = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xC0, 0, 0 };
        private byte[] DisconnectProtocol = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xC1, 0, 0 };

        /* Device Information command */
        private byte[] InfoGetProductIDCommand = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xA0, 0, 0 };
        private byte[] InfoGetHWVersionCommand = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xA1, 0, 0 };
        private byte[] InfoGetFWVersionCommand = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xA2, 0, 0 };
        private byte[] InfoGetBLVersionCommand = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xA3, 0, 0 };
        private byte[] InfoGetActiveCodeCommand = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xA9, 0, 0 };
        private byte[] InfoSetProductIDCommand = new byte[50];
        private byte[] InfoSetProductActiveCode = new byte[50];

        /* User Information command */
        private byte[] InfoGetUserNameCommand       = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xA4, 0, 0 };
        private byte[] InfoGetUserIDCommand         = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xA5, 0, 0 };
        private byte[] InfoGetUserTeleNumbCommand   = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xA6, 0, 0 };
        private byte[] InfoGetUserMailCommand       = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xA7, 0, 0 };
        private byte[] InfoGetUserAccPassCommand    = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xA8, 0, 0 };

        private byte[] InfoSetUserNameCommand = new byte[50];
        private byte[] InfoSetUserIDCommand = new byte[50];
        private byte[] InfoSetUserTeleNumbCommand = new byte[50];
        private byte[] InfoSetUserMailCommand = new byte[50];
        private byte[] InfoSetUserAccPassCommand = new byte[50];

        /* User Profile command */
        private byte[] InfoGetPhaseXSPKCommand      = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xAA, 0, 1, 1};
        private byte[] InfoSetPhaseXSPKCommand      = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xBA, 0, 3, 1, 0, 0};
        private byte[] InfoGetPhaseXVIBCommand      = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xAB, 0, 1, 1 };
        private byte[] InfoSetPhaseXVIBCommand      = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xBB, 0, 3, 1, 0, 0 };
        private byte[] InfoGetPhaseTimeUsingCommand = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xAC};
        private byte[] InfoSetPhaseTimeUsingCommand = new byte[20];
        private byte[] InfoUserProfileActive        = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xAD };
        private byte[] InfoUserProfileDeactive      = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xBD };

        /* Media Control command */
        private byte[] MediaPlayStopCommand         = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xE0, 0, 0 };
        private byte[] MediaPlayPauseCommand        = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xE6, 0, 0 };
        private byte[] MediaNextCommand             = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xE1, 0, 0 };
        private byte[] MediaPreviousCommand         = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xE2, 0, 0 };
        private byte[] MediaIncreaseVolumeCommand   = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xE3, 0, 0 };
        private byte[] MediaDecreaseVolumeCommand   = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xE4, 0, 0 };
        private byte[] SongSelectionCommand         = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xAE, 1, 0, 0, 0 };
        private byte[] A2DPCommand                  = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xAF, 0, 0};
        private byte[] GetSpeakerLevel              = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xE7, 0, 0 };
        private byte[] SetSpeakerLevel = new byte[50];
        private byte[] GetVibratorLevel             = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xF7, 0, 0 };
        private byte[] SetVibratorLevel = new byte[50];

        /* Vibrator Control command */
        private byte[] VibratorPlayStopCommand = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xF0, 0, 0 };
        private byte[] VibratorWeakCommand = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xF2, 0, 0 };
        private byte[] VibratorStrongCommand = { 0x41, 0x54, 0x44, 0x57, 0x00, 0x01, 0xF1, 0, 0 };

        /* Data variable */
        private string StrDataInput;
        private byte[] StrDataAddToFrame;

        /* For graph Speaker */
        private PlotView Speakerview;
        /* For graph Vibrator */
        private PlotView Vibratorview;

        /* For protocol*/
        private bool bFlagSendData = false;
        private bool bFlagIntervalTimeout = false, bFlagPackageReady = false;
        private int uCounterTimer = 0;

        /* For check 1st byte of header frame */
        int  bIndex = 0;
        byte bCheck1stHeaderByte =0;
        int  bCounterGetPackageHeader = 0;
        int  bFlagStartGetHeader = 0;
        bStateMachineReceivingData bStateFindPackageInRingBuffer = bStateMachineReceivingData.eSM_Step0;
        int bCurrentSequence = 0, bLastSequence = 0, bMissingRealPackageCounter=0;
        byte[] strSequence = new byte[2];
        MessageIds MSG_ID = 0;
        string strMissingSequence;
        /* Data value */
        float EEGTP9, EEGFP1, EEGFP2, EEGTP10;
        float GYRO_X, GYRO_Y, GYRO_Z, ACCE_X, ACCE_Y, ACCE_Z;

        private bool bFlagGetUserDataInput = false;
        /// <summary>
        /// Gets the plot controller.
        /// </summary>
        public IPlotController Controller { get; private set; }

        /* Component Index */
        private Component_Index pComIndex;
        private enum Component_Index
        {
            /* Information index */
            CoIdx_ProductID = 1,
            CoIdx_UserName  = 2,
            CoIdx_UserID    = 3,
            CoIdx_UserPass  = 4,
            CoIdx_UserTel   = 5,
            CoIdx_UserMail  = 6,
            CoIdx_ProductActiveCode = 7,

            CoIdx_SpeakerLevelPhase1= 8,
            CoIdx_SpeakerLevelPhase2 = 9,
            CoIdx_SpeakerLevelPhase3 = 10,
            CoIdx_SpeakerLevelPhase4 = 11,
            CoIdx_SpeakerLevelPhase5 = 12,

            CoIdx_SpeakerTimingPhase1 = 13,
            CoIdx_SpeakerTimingPhase2 = 14,
            CoIdx_SpeakerTimingPhase3 = 15,
            CoIdx_SpeakerTimingPhase4 = 16,
            CoIdx_SpeakerTimingPhase5 = 17,

            CoIdx_VibratorLevelPhase1 = 18,
            CoIdx_VibratorLevelPhase2 = 19,
            CoIdx_VibratorLevelPhase3 = 20,
            CoIdx_VibratorLevelPhase4 = 21,
            CoIdx_VibratorLevelPhase5 = 22,

            CoIdx_VibratorTimingPhase1 = 23,
            CoIdx_VibratorTimingPhase2 = 24,
            CoIdx_VibratorTimingPhase3 = 25,
            CoIdx_VibratorTimingPhase4 = 26,
            CoIdx_VibratorTimingPhase5 = 27,

            CoIdx_TimeUsing = 28,
            CoIdx_SpeakerLevel = 29,
            CoIdx_VibratorLevel = 30,
        }
        private enum Cmd_Type
        {
            /*Use for PC To TAG*/
            P2TCMD_CONNECT = 0xC0,
            P2TCMD_DISCONNECT = 0xC1,
            P2TCMD_START_DOWNLOAD_FIRMWARE = 0xC2,
            P2TCMD_DOWNLOAD_FIRMWARE = 0xC3,
            P2TCMD_END_DOWNLOAD_FIRMWARE = 0xC4,
            P2TCMD_RUN_FIRMWARE = 0xC5,
            P2TCMD_GET_CHECKSUM = 0xC6,

            /* Config Command  */
            P2TCMD_SET_CONFIG = 0xD1,/*Only Use for PC*/
            P2TCMD_GET_CONFIG = 0xD2,/*Only Use for PC*/

            /* Support Smart Pillow */
            /* Get Information about device */
            P2TCMD_GET_PRODUCT_ID = 0XA0,
            P2TCMD_GET_HW_VER = 0XA1,
            P2TCMD_GET_FW_VER = 0XA2,
            P2TCMD_GET_BL_VER = 0XA3,
            P2TCMD_GET_PRODUCT_ACTIVE = 0XA9,
            /* Set Information about device */
            P2TCMD_SET_PRODUCT_ID = 0XB0,
            P2TCMD_SET_HW_VER = 0XB1,
            P2TCMD_SET_FW_VER = 0XB2,
            P2TCMD_SET_BL_VER = 0XB3,
            P2TCMD_SET_PRODUCT_ACTIVE = 0XB9,

            /* Get Information about user */
            P2TCMD_GET_USER_NAME = 0XA4,
            P2TCMD_GET_USER_ID = 0XA5,
            P2TCMD_GET_USER_TELE_NUM = 0XA6,
            P2TCMD_GET_USER_MAIL = 0XA7,
            P2TCMD_GET_USER_ACCPASS = 0XA8,

            /* Set Information about user */
            P2TCMD_SET_USER_NAME = 0XB4,
            P2TCMD_SET_USER_ID = 0XB5,
            P2TCMD_SET_USER_TELE_NUM = 0XB6,
            P2TCMD_SET_USER_MAIL = 0XB7,
            P2TCMD_SET_USER_ACCPASS = 0XB8,

            /* For Media control in SD card */
            P2TCMD_MEDIA_PLAYSTOP = 0XE0,
            P2TCMD_MEDIA_NEXT = 0XE1,
            P2TCMD_MEDIA_PREVIOUS = 0XE2,
            P2TCMD_MEDIA_INCREASE_VOL = 0XE3,
            P2TCMD_MEDIA_DECREASE_VOL = 0XE4,
            P2TCMD_MEDIA_TIMEPLAY = 0xE5,
            P2TCMD_MEDIA_GET_SPEAKER_LEVEL = 0xE7,
            P2TCMD_MEDIA_SET_SPEAKER_LEVEL = 0xE8,

            /* For Vibrator control */
            P2TCMD_VIB_PLAYSTOP = 0XF0,
            P2TCMD_VIB_INCREASE_INT = 0XF1,
            P2TCMD_VIB_DECREASE_INT = 0XF2,
            P2TCMD_VIB_GET_VIBRATOR_LEVEL = 0xF7,
            P2TCMD_VIB_SET_VIBRATOR_LEVEL = 0xF8,

            /* User Profile */
            P2TCMD_GET_SPEAKER_PHASE_X = 0XAA,
            P2TCMD_SET_SPEAKER_PHASE_X = 0XBA,
            P2TCMD_GET_VIBRATOR_PHASE_X = 0XAB,
            P2TCMD_SET_VIBRATOR_PHASE_X = 0XBB,

            /* TIme Using */
            P2TCMD_GET_TIME_USING = 0XAC,
            P2TCMD_SET_TIME_USING = 0XBC,

            /* Song duration and selection */
            P2TCMD_SONG_SELECTION = 0XAE,
            P2TCMD_GET_A2DP_STATE = 0XAF,
        };

        /* Frame index */
        const int iUART_PREAM_OFFSET_1 = 0;
        const int iUART_PREAM_OFFSET_2 = 1;
        const int iUART_PREAM_OFFSET_3 = 2;
        const int iUART_PREAM_OFFSET_4 = 3;
        const int iUART_LENGTH_OFFSET  = 4;
        const int LENGTH_SIZE = 2;
        const int iUART_SIZE_LOW = 4;
        const int iUART_SIZE_HIGH = 5;
        const int iUART_CMD = 6;
        const int iUART_IDX_LOW = 7;
        const int iUART_IDX_HIGH = 8;
        const int iUART_DATA = 9;
        const int iUART_DATA_PACKAGE = 11;

        /* Size of field */
        const int PRODUCT_NAME_SIZE = 20;
        const int PRODUCT_ID_SIZE = 4;
        const int FW_VERSION_SIZE = 12;
        const int BL_VERSION_SIZE = 12;
        const int HW_VERSION_SIZE = 12;
        const int EMERGENCY_CODE_SIZE = 5;
        const int CONFIG_PARA_SIZE = 2;
        const int SIZE_ACTIVE_CODE = 4;
        const int SIZE_PHONE_NUMBER = 15;
        /* For user info */
        const int SIZE_USER_NAME = 20;
        const int SIZE_ID_ACCOUNT = 10;
        const int SIZE_ACCOUNT_PASSWORD = 18;
        const int SIZE_USER_TELEPHONE = 16;
        const int SIZE_USER_MAIL = 30;
        const int SIZE_PHASE = 2;//1 byte for timing, 1 byte for level
        const int ALL_SIZE = 256;

        /**/
        int USING_SPP_BLE = 1; /* 0 is SPP, 1 is BLE */
        #endregion
    }

    public class BluetoothConnection
    {
        public void getAdapter() { this.thisAdapter = BluetoothAdapter.DefaultAdapter; }
        //public void getDevice() { this.thisDevice = (from bd in this.thisAdapter.BondedDevices where bd.Name == "DeltawaveBand-102" select bd).FirstOrDefault(); }
        public void getDevice(string bDeviceName) { this.thisDevice = (from bd in this.thisAdapter.BondedDevices where bd.Name == bDeviceName select bd).FirstOrDefault(); }
        public BluetoothAdapter thisAdapter { get; set; }
        public BluetoothDevice thisDevice { get; set; }
        public BluetoothSocket thisSocket { get; set; }
    }

}

